import jsxMd from "../../src/helpers/jsxMd";
import React from "react";

export default {
	meta: {
		title: "f.lux",
		type: "article",
		date: "2016-05-28",
		tagline: "Quick tour of f.lux, a color adjusting application",
		image: "flux.png",
		category: "geek"
	},
	content: jsxMd`Trying to improve my sleeping time, and remembering an [old friend ](https://github.com/camillobruni) using it, I decided to give a try to [f.lux](https://justgetflux.com/). Here is a quick tour of it.

## Introduction

[f.lux](https://justgetflux.com/) is a software which adjusts your screen's color temperature according to the date and time, and your geolocation. The purpose is to have your screen matching the ambient light at day **and** at night in order to reduce eye strain during night time and to improve your sleep time. A couple of studies (some can be found on [their website](https://justgetflux.com/research.html)) shows that reducing exposure to a blue glowing screen at night improves sleep effectiveness.

## Setup

The first time you launch the app, your location will be asked in order to adjust the timezone. It is more or less the only thing that you need to specify to start using the app (it can be nice to adjust the waking time though).

${<a className="popup-link" href="/images/post/flux/settings.png" title="f.lux setting page"/>}

As you can see above the setting page does not have that much tweaking possibilities, but it does the job.
The only change I made since I have the app is to the color schema to have it a bit less yellow at night.

Once setup, it's easy to forget about the app as it does its job without burden.

In addition to its main feature, *f.lux* proposes to notify you when it is time to go to bed. It may sound fishy but it works (at least on me).

${<a className="popup-link" href="/images/post/flux/before-after.png" title="f.lux in action"/>}

## Pros & Cons

${<table className="pros-n-cons">
		<thead>
		<tr>
			<td>Pros</td>
			<td>Cons</td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>Gratis</td>
			<td>Proprietary</td>
		</tr>
		<tr>
			<td>Reduce eye strain</td>
			<td>Collect data (position)</td>
		</tr>
		<tr>
			<td>Easy to setup</td>
			<td/>
		</tr>
		<tr>
			<td>Notify when you should go to bed</td>
			<td/>
		</tr>
		</tbody>
	</table>}

## Avaibility

This application is available on GNU/Linux, Windows, Mac OS X, iPhone/iPad and Android. Note that it requires the iPhone/iPad to be jailbreaked and the Android phone to be rooted.

An unofficial port to Chrome plugin exists for those using Chrome OS. It's named *G.lux* and can be found [here](https://chrome.google.com/webstore/detail/glux/hinolicfmhnjadpggledmhnffommefaf).

## Conclusion

If you are just curious, or want to improve your sleep efficiency at low cost, *f.lux* can be a nice app to try.

### References

- [f.lux Official page](https://justgetflux.com/)
- [f.lux Wikipedia page](https://en.wikipedia.org/wiki/F.lux)
- [Redshift Wikipedia page](https://en.wikipedia.org/wiki/Redshift_(software)): A free software inspired by f.lux for BSD, GNU/Linux, and Windows.
- [Orioto page](http://orioto.deviantart.com/): If you liked my background image.
`
};
