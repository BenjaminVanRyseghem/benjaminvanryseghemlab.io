export default {
	meta: {
		type: "album",
		album: "Les Funérailles d'antan",
		artist: "Georges Brassens",
		date: "2021-02-14",
		song: "L'Orage",
		url: "https://music.youtube.com/browse/MPREb_3P0iavU1ObJ",
		description: "Brassens was one of the latest great french poet. This album contains great lyrics",
		discovered: "My parents are big fans of Georges Brassens"
	}
};
