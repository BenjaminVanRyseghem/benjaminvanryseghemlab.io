export default {
	meta: {
		type: "album",
		album: "L'Atelier",
		artist: "Tarmac",
		date: "2021-05-02",
		song: "Dis moi c'est quand...",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lIT-nNGETk4iOucv8w7n5WqZ9Us3h_KwQ",
		description: "First album after the split of Louise Attaque",
		discovered: "As a fan of Louise Attaque, it was a must have, and I was not disappointed"
	}
};
