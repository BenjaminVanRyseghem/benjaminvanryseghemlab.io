export default {
	meta: {
		type: "album",
		album: "Wish You Were Here",
		artist: "Pink Floyd",
		song: "Wish You Were Here",
		date: "2020-09-27",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mVPAJe_QHbqx9AMXNT2noHhm6vtf3bANQ",
		description: "To me, this is one of the most iconic Pink Floyd albums. The eponymous song is the most beautiful ballad I ever heard",
		discovered: "My dad being a huge fan of Pink Floyd, I feel like I have always heard this group for my whole life"
	}
};
