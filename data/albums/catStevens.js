export default {
	meta: {
		type: "album",
		album: "Tea For The Tillerman",
		artist: "Cat Stevens",
		date: "2020-11-29",
		song: "Father And Son",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nO344Eg2zes1vEKpkFjZ_YantNvxvHUAI",
		description: "Tea For The Tillerman is a nice album to chill while listening to songs we all have eared a million time, in a cozy 70's atmosphere",
		discovered: "My mom was listening to Cat Stevens more or less my whole childhood"
	}
};
