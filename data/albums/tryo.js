export default {
	meta: {
		type: "album",
		album: "Mamagubida",
		artist: "Tryo",
		date: "2021-05-30",
		song: "La misère d'en face",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lJCbrDOafpNzxZU-ew4x3o3VOV9BFGDBs",
		description: "A first album that smells like summer, sun and BBQ",
		discovered: "This album was very famous among teens when I was in high-school"
	}
};
