export default {
	meta: {
		type: "album",
		album: "A la panxa del bou",
		artist: "La Troba Kung-Fú",
		date: "2021-06-06",
		song: "Subway Walk",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_l1NQQMDz75RkXv-7Q5onC3QtC-f7lHXqk",
		description: "Good vibes and joyful music",
		discovered: "This group run the first-part of Manu Chao tour a couple of years ago"
	}
};
