export default {
	meta: {
		type: "album",
		album: "Toxicity",
		artist: "System of a Down",
		date: "2021-05-16",
		song: "Needles",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_n1sOLxWCfeedpeiN-YS7H5xJq5Qptac-M",
		description: "Second album for SOAD, it contains everything that makes them famous",
		discovered: "I was in high-school when this album was released, and everyone was listening to this"
	}
};
