export default {
	meta: {
		type: "album",
		album: "Origin of Symmetry",
		artist: "Muse",
		date: "2021-07-04",
		song: "Citizen Erased",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nLOnIt5az61ZWWPtNpfInWM5jJnJpyVhU",
		description: "Second album for Muse and to me their best (tight with Showbiz)",
		discovered: "My best friend is **huge** fan of Muse, and thanks to him I listened to this album through highschool"
	}
};
