export default {
	meta: {
		type: "album",
		album: "The Link",
		artist: "Gojira",
		song: "Embrace the world",
		date: "2021-08-22",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_laEFOy5mp_ON5x4mUZibYxtpEQJiBvS_E",
		description: "Second album of the group that will soon be the greatest french metal band",
		discovered: "My older brother was fan, and brought me to a couple of their concerts"
	}
};
