export default {
	meta: {
		type: "album",
		album: "Dream",
		artist: "Kitaro",
		date: "2021-05-23",
		song: "Lady of Dreams (feat. Jon Anderson)",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lhg3R_kOVr7wBTK8P0Z2ThnA_SX25nRVo",
		description: "A nice fusion of japanese traditional instruments with Kitaro and the clear voice Jon Anderson",
		discovered: "My dad being a fan of Yes, it was just a matter of time"
	}
};
