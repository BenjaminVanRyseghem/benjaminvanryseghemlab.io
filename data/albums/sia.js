export default {
	meta: {
		type: "album",
		album: "1000 Forms of Fear",
		artist: "Sia",
		date: "2021-07-25",
		song: "Fire Meet Gasoline",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_kSTiHRyCsUGTrvIcNa3XBxITERYhmmMaw",
		description: "Most famous album of Sia, full of awesome songs, and beautiful video clips",
		discovered: "Chandelier was such a shock, I dived deep into this album"
	}
};
