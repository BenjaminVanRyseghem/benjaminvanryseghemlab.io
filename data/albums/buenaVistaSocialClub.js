export default {
	meta: {
		type: "album",
		album: "Buena Vista Social Club",
		artist: "Buena Vista Social Club",
		date: "2020-11-15",
		song: "Chan Chan",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mVkrPqX7FdfhLfatFUF438DYPqPUfanrA",
		description: "This album is probably the most famous Cuban music album reviving the music of pre-revolutionary Cuba",
		discovered: "My dad was listening to this album a lot a couple of years ago"
	}
};
