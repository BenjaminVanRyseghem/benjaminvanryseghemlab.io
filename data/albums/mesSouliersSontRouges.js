export default {
	meta: {
		type: "album",
		album: "Ce qui nous lie",
		artist: "Mes Souliers Sont Rouges",
		date: "2020-10-04",
		song: "La barbière",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lhBRALwtib2EKSyU-9cllWA1e2FwlR-I0",
		description: "MSSR is back after almost 14 years with a new line-up composed of both former members and new ones. The band is back to its roots and focus here on old and almost forgotten Norman songs",
		discovered: "I discovered MSSR through a friend of mine back in high school. So when I saw they were performing live. I immediately bought tickets for the whole family to a [festival](https://www.festival-picarts.com/album-photos-2019/album-photos-2018/) nearby"
	}
};
