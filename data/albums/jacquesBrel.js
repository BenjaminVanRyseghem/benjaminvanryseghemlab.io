export default {
	meta: {
		type: "album",
		album: "Jacques Brel 67",
		artist: "Jacques Brel",
		date: "2020-12-20",
		song: "La chanson des vieux amants",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_m9gb4CrM01vi8EXw9W9BZkdWT3rxPaQNU",
		description: "An iconic album from \"The Greatest Belgian\"",
		discovered: "My father is and always has been a huge fan of Brel"
	}
};
