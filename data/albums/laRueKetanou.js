export default {
	meta: {
		type: "album",
		album: "En attendant les caravanes...",
		artist: "La Rue Kétanou",
		date: "2020-12-13",
		song: "La fiancée de l'eau",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_loaPYsg-pm2bpQKLPXTdDc9gMABNOQrLo",
		description: "A very good french album with powerful lyrics and old-fashioned instrumental",
		discovered: "A friend make me listen to it in high-school"
	}
};
