export default {
	meta: {
		type: "album",
		album: "Holy Wood",
		artist: "Marilyn Manson",
		song: "Count To Six And Die",
		date: "2021-08-15",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_kGMDW55V_5cF9te_-ptUJEJRo6ujB3oEY",
		description: "My favorite Manson album, going back to its metal roots after a more glam-rock album",
		discovered: "This album spreads quite massively among metal listeners"
	}
};
