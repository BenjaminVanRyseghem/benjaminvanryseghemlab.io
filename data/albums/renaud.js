export default {
	meta: {
		type: "album",
		album: "Mistral gagnant",
		artist: "Renaud",
		date: "2020-11-22",
		song: "P'tite conne",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_kUHovH8P__RO1_m0pM8S3zBVkgpkMc8R4",
		description: "This album is to me representative of the second part of Renaud career with more romantic songs",
		discovered: "My dad is a fan of Renaud, and his great lyrics really hooked me up"
	}
};
