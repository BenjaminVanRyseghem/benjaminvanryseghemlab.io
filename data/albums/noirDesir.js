export default {
	meta: {
		type: "album",
		album: "666.667 Club",
		artist: "Noir Désir",
		date: "2021-03-07",
		song: "À ton étoile",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mHP1lBe8_q7anYNU0O9b8S5B_AsuWJh18",
		description: "Cool rock french album,  with a couple of classics!",
		discovered: "Noir Désir was pretty popular in the late 90's"
	}
};
