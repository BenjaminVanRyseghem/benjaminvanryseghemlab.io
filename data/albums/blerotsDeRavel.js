export default {
	meta: {
		type: "album",
		album: "Vol au-dessus d'un nid de cagoules",
		artist: "Les Blerots de R.A.V.E.L.",
		song: "Embrace the world",
		date: "2021-08-29",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mRqSjakzaAFLjtqzKtt4oH0MzSUpyBnM8",
		description: "It's a weird band, mixing loony lyrics and easter-europe instruments. Surprisingly the result is delightful",
		discovered: "I Discover This Band At A Local Musical Festival, *[les Mélomanies](https://www.facebook.com/melomanies)*"
	}
};
