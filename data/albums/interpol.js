export default {
	meta: {
		type: "album",
		album: "Antics",
		artist: "Interpol",
		date: "2020-11-08",
		song: "Evil",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nXILrzczCNax2fIwykdva9eENEbrInn94",
		description: "It's quite a dark rock album with a touch of British rock yet it's American",
		discovered: "An ex-girlfriend was fond of this album"
	}
};
