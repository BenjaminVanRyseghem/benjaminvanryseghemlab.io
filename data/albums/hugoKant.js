export default {
	meta: {
		type: "album",
		album: "Far From Home",
		artist: "Hugo Kant",
		date: "2020-12-06",
		song: "Before Midnight Tonight",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lxKADehcYCIkXD14pXg-hwkUdVgW5ifnw",
		description: "This is a nice ambient music album I like that I like to listen while working",
		discovered: "YouTube suggested it to me"
	}
};
