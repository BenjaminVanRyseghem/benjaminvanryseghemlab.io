export default {
	meta: {
		type: "album",
		album: "Clandestino",
		artist: "Manu Chao",
		date: "2020-12-27",
		song: "Minha galera",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nXEwm51YzGk88huvIVMIZ6OVI_55j0RtU",
		description: "First solo album for Manu Chao, it is a nice \"summer album\" full of good vibes",
		discovered: "It was on all the radios when it came out, no way to avoid it"
	}
};
