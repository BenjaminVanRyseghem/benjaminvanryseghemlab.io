export default {
	meta: {
		type: "album",
		album: "Alive 2007",
		artist: "Daft Punk",
		song: "Around the World / Harder, Better, Faster, Stronger",
		date: "2021-08-08",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mifAgKvEjBwDijGc8kDCWb5tkFWEgUW3c",
		description: "The last live album of Daft Punk, with all their hits and awesome tracks",
		discovered: "My best friend is a huge fan, and introduced me to this band"
	}
};
