export default {
	meta: {
		type: "album",
		album: "L'Apocalypse des animaux",
		artist: "Vangelis",
		date: "2021-01-03",
		song: "La petite fille de la mer",
		url: "https://music.youtube.com/browse/MPREb_P6HCdAlnbjq",
		description: "A nice instrumental album made for an documentary about the animal kingdom",
		discovered: "My parents used to play this album as a lullaby for my siblings and I"
	}
};
