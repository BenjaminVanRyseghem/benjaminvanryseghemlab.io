export default {
	meta: {
		type: "album",
		album: "Fragile",
		artist: "Francis Cabrel",
		date: "2021-06-27",
		song: "L'encre de tes yeux",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mOrW4vXOSMP30mbqAYOGDPbb49Da_wDq0",
		description: "One of the last french poet, the lyrics are just sublimes",
		discovered: "My parents had a couple of his albums, and I jump into it quite young"
	}
};
