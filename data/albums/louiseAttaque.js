export default {
	meta: {
		type: "album",
		album: "Louise Attaque",
		artist: "Louise Attaque",
		date: "2021-01-17",
		song: "Cracher nos souhaits",
		url: "https://music.youtube.com/browse/MPREb_n7L3h5OE5WV",
		description: "First album for Louise Attaque and Gaëtan Roussel, this album is a great french rock album from the 90's",
		discovered: "This album was all over the radios when I was in middle school"
	}
};
