export default {
	meta: {
		type: "album",
		album: "Gorillaz",
		artist: "Gorillaz",
		date: "2021-03-14",
		song: "M1 A1",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nnPhHNMcLdaMCeHPJ9QOWuMOJzIggILTs",
		description: "A mix of rock, pop, with some rap in it! Awesome first album",
		discovered: "When it was released, this album was a real tsunami"
	}
};
