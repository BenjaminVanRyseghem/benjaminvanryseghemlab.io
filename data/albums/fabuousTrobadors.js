export default {
	meta: {
		type: "album",
		album: "Duels de tchatche",
		artist: "Fabulous Trobadors",
		date: "2021-06-13",
		song: "Duel de sans-pareil",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mqJNqiNJ6MdLkZzLsByeAS5dQFFBYl9As",
		description: "Revision of traditional music style from the South West of France, with a mix of brazilian music",
		discovered: "I actually can't remember"
	}
};
