export default {
	meta: {
		type: "album",
		album: "Universal Mother",
		artist: "Sinéad O'Connor",
		date: "2021-01-10",
		song: "Thank you for hearing me",
		url: "https://music.youtube.com/browse/MPREb_hN1Ul5zz2pe",
		description: "An album packed with a lot of great songs, and still the powerful voice of Sinéad O'Connor",
		discovered: "After \"Nothing Compares to U\" I dived in the whole discography of Sinéad O'Connor with no regrets"
	}
};
