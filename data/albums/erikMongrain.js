export default {
	meta: {
		type: "album",
		album: "Fates",
		artist: "Erik Mongrain",
		date: "2020-10-25",
		song: "Percusienfa",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lT8jwBadlKR0blRrSBvCrjy_-AN1fmeIw",
		description: "This is a vibrant instrumental only album. It is worth watching a few video clip as Erik Mongrain has a technique of his own",
		discovered: "Through a friend back in university"
	}
};
