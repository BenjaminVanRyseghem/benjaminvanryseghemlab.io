export default {
	meta: {
		type: "album",
		album: "'Til Shiloh",
		artist: "Buju Banton",
		date: "2020-10-18",
		song: "Champion",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_moo7pbQERbr7yIibCbjxUxXWD0tP_94bk",
		description: "A ragga album with a very deep voice",
		discovered: "I can not remember"
	}
};
