export default {
	meta: {
		type: "album",
		album: "Space Oddity",
		artist: "David Bowie",
		date: "2021-02-28",
		song: "Space Oddity",
		url: "https://music.youtube.com/browse/MPREb_vuDjY0fElhQ",
		description: "Not yet a super star, this album shows all the premises of how Bowie will contributes to the music",
		discovered: "I can't really remember as Bowie is such great artist that he is everywhere"
	}
};
