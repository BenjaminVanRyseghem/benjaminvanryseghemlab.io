export default {
	meta: {
		type: "album",
		album: "Blood Sugar Sex Magik",
		artist: "Red Hot Chili Peppers",
		date: "2021-01-24",
		song: "I Could Have Lied",
		url: "https://music.youtube.com/browse/MPREb_j7deMtIDHAx",
		description: "One of the more iconic album of the Red Hot Chili Peppers to me, packed with a ton of hits",
		discovered: "A friend in high school was very fond of them, and of Flea's bassline"
	}
};
