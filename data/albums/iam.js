export default {
	meta: {
		type: "album",
		album: "L’École du micro d’argent",
		artist: "IAM",
		date: "2021-02-07",
		song: "Nés sous la même étoile",
		url: "https://music.youtube.com/browse/MPREb_04nCcoBsgp9",
		description: "This is a must-have rap album from the late 90's. Probably the best IAM album",
		discovered: "As a teenager in the late 90's, no one could avoid this album, for the greater good"
	}
};
