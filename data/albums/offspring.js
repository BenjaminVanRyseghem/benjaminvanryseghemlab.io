export default {
	meta: {
		type: "album",
		album: "Americana",
		artist: "The Offspring",
		date: "2020-10-11",
		song: "The Kids Aren't All Right",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_l5fgOue50rWIy5G8WC6hF_r7oY1Se5hiU",
		description: "Probably one of the most famous Californian punk album, Americana is to me the best Offspring album",
		discovered: "I have been introduced to this album by my best friend back in early high-school. It has been with us during a lot of climbing trips"
	}
};
