export default {
	meta: {
		type: "album",
		album: "Live / Unplugged",
		artist: "Alanis Morissette",
		date: "2021-06-20",
		song: "King of Pain",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_njGA1UQBQdDkPznTOr6vLS_KG3FpVW_uE",
		description: "Great live album where Alanis Morissette gives us one of her best performance",
		discovered: "*Ironic* and *Thank U* made Alanis Morissette one of the top singer of the 90's"
	}
};
