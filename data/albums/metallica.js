export default {
	meta: {
		type: "album",
		album: "Master Of Puppets",
		artist: "Master Of Puppets",
		date: "2021-04-18",
		song: "Battery",
		url: "https://music.youtube.com/channel/MPREb_EvUaRykfAC1",
		description: "The album that brought Metallica to fame and is considered as one of the greatest heavy metal album of all time",
		discovered: "A couple of good friends are really fan"
	}
};
