export default {
	meta: {
		type: "album",
		album: "Mezzanine",
		artist: "Massive Attack",
		song: "Teardrop",
		date: "2021-08-01",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nd4LborPNxR-W0gX3HkfJP9LTTSov6gG0",
		description: "Probably the most known album of Massive Attack, and for a good reason",
		discovered: "I do not remember"
	}
};
