export default {
	meta: {
		type: "album",
		album: "En Solitaire",
		artist: "William Sheller",
		date: "2021-04-11",
		song: "Chanson lente",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nzRIabTJMQNJUNeUbtjN_nGNZmcRxAyDs",
		description: "An acoustic album of William Sheller that is a turn in is discography",
		discovered: "My parents introduced me to William Sheller in my early years"
	}
};
