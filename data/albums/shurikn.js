export default {
	meta: {
		type: "album",
		album: "Où je vis",
		artist: "Shurik'n",
		date: "2021-07-11",
		song: "La lettre",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_n_8y6M2NP0VKanHDMjvn8y26aqfc_XH5A",
		description: "First solo album for one of the leader of Iam",
		discovered: "I discovered it quite recently when diving again in Iam albums"
	}
};
