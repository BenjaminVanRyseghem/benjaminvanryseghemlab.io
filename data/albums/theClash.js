export default {
	meta: {
		type: "album",
		album: "London Calling",
		artist: "The Clash",
		date: "2021-01-31",
		song: "Train in Vain (Stand by Me)",
		url: "https://music.youtube.com/browse/MPREb_yeJ9gspPXeL",
		description: "Just after the Sex Pistol revolution, and the death of Sid Vicious, The Clash raised with this album, bringing the London punk all over the world",
		discovered: "I can't remember, as this group, and this album are considered classics"
	}
};
