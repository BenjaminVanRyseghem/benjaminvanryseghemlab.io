export default {
	meta: {
		type: "album",
		album: "Elect the Dead",
		artist: "Serj Tankian",
		date: "2021-03-21",
		song: "Baby",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_mYX3o906w3FyUA9SGh67_EZq0J2zODQCs",
		description: "First solo  album from the lead singer of System Of A Down, instant hit!",
		discovered: "As a huge fan of SOAD, I could miss this album, and I regret nothing"
	}
};
