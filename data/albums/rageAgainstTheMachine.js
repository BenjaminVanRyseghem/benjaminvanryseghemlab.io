export default {
	meta: {
		type: "album",
		album: "Rage Against the Machine",
		artist: "Rage Against the Machine",
		date: "2020-11-01",
		song: "Killing In the Name",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nBHPxu_x10TYhO4vbP895bER4a70ru0aI",
		description: "As a powerful rock album with great political lyrics, this first album was announcing a great future for this band",
		discovered: "My older brother was listening to this album when I was in middle school, and I remembered it a couple of years later"
	}
};
