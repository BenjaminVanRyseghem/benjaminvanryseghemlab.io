export default {
	meta: {
		type: "album",
		album: "Maniacx",
		artist: "Maniacx",
		date: "2021-03-28",
		song: "Flik Flak In The Party",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_lZqH8i35w6f4jLhV3DzeMSWcoz6HjEX4A",
		description: "French hip-hop band with a very funny vibe",
		discovered: "A friend made me discovered this album during a summer road trip"
	}
};
