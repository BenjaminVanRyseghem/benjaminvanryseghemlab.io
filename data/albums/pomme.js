export default {
	meta: {
		type: "album",
		album: "Les Failles",
		artist: "Pomme",
		date: "2021-05-09",
		song: "Saphir",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_n0BablVOQNGpSjgwwLC5VOmZgMtgn3rmM",
		description: "Beautiful and full of melancholia",
		discovered: "I discovered Pomme in a youtube video named \"301 vues\""
	}
};
