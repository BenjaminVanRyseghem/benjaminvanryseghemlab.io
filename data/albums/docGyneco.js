export default {
	meta: {
		type: "album",
		album: "Première consultation",
		artist: "Doc Gyneco",
		date: "2021-04-04",
		song: "Nirvana",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_nrsktxHSRvMb110XdHUZz0lel9F8bwHuw",
		description: "First Doc Gyneco album",
		discovered: "I discovered this album with its 20th anniversary edition"
	}
};
