export default {
	meta: {
		type: "album",
		album: "No Need To Argue",
		artist: "The Cranberries",
		date: "2021-04-25",
		song: "No Need To Argue",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_kYlNzjht7ieFooYAaC-3FUgH2qlQ-C30k",
		description: "Most iconic The Cranberries album",
		discovered: "With Zombie being the internation hit we all know about, it was just a mater of time before bumping into this album"
	}
};
