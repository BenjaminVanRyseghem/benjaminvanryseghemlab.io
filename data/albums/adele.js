export default {
	meta: {
		type: "album",
		album: "21",
		artist: "Adele",
		date: "2021-07-18",
		song: "One And Only",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_kNiuS8m9gEwAoTTt62VFPFwPFMdqDC8Oo",
		description: "Second album of Adele which proved to the entire world the great artist she is",
		discovered: "I can't really remember, but this album was all over the air when it released"
	}
};
