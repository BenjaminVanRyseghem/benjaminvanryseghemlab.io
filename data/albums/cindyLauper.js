export default {
	meta: {
		type: "album",
		album: "She's So Unusual",
		artist: "Cyndi Lauper",
		date: "2021-02-21",
		song: "Time After Time",
		url: "https://music.youtube.com/browse/MPREb_wRKmB04VUmW",
		description: "A feel-good album full of love songs from the 90's",
		discovered: "A friend of mine is really fond of Cyndi Lauper, and time after time (pun intended) I learned to love her songs"
	}
};
