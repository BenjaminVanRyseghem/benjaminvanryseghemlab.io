export default {
	meta: {
		type: "album",
		album: "ダ ン テ",
		artist: "Kalm",
		date: "2020-09-20",
		url: "https://music.youtube.com/playlist?list=OLAK5uy_k8qv4DoCvgiR1rdR41dkra9s5qA6PI3zU",
		song: "Leaving",
		description: "Relaxing pop/electro album fully instrumental. I like to listen to this album to chill or as background music while I cook",
		discovered: "I discovered it when my wife asked for \"calm music\" and as a smart-ass, I searched \"calm\" in Google Music"
	}
};
