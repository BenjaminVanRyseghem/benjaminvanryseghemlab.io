/* eslint-disable filenames/match-regex */
const path = require("path");

module.exports = {
	resolve: {
		modules: [
			path.resolve(process.cwd(), "src"),
			"node_modules"
		]
	}
};
