import "./app.css";
import { Root, Routes } from "react-static";
import React from "react";
import { Router } from "@reach/router";

export default class App extends React.Component {
	render() {
		return (
			<Root>
				<div className="content">
					<React.Suspense fallback={<em>Loading...</em>}>
						<Router>
							<Routes path="*"/>
						</Router>
					</React.Suspense>
				</div>
			</Root>
		);
	}
}
