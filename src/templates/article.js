import React, { useEffect } from "react";
import ArticleModel from "models/article";
import { useRouteData } from "react-static";
import withFooter from "hoc/withFooter";
import withHeader from "hoc/withHeader";

function Article() {
	const { entry: entryData } = useRouteData();
	let article = ArticleModel.materialize(entryData);
	let bodyCount = article.bodyCount();

	useEffect(() => {
		// eslint-disable-next-line camelcase,func-style
		let disqus_config = function() {
			// eslint-disable-next-line no-invalid-this
			this.page.url = `https://benjamin.vanryseghem.com${article.url()}`; // Replace PAGE_URL with your page's canonical URL variable
			// eslint-disable-next-line no-invalid-this
			this.page.identifier = article.id(); // Replace PAGE_IDENTIFIER with your page's unique identifier variable
		};

		(function iife() { // DON'T EDIT BELOW THIS LINE
			let doc = document;
			let script = doc.createElement("script");
			script.src = "//benjaminvanryseghem.disqus.com/embed.js";

			script.setAttribute("data-timestamp", +new Date());
			(doc.head || doc.body).appendChild(script);
		})();
	}, []);

	return (
		<>
			<div className="outer" id="header_wrap">
				<header className="inner">
					<h1 id="project_title">{article.title()}</h1>
					<span id="project_tagline">{article.tagline()}</span>
					<div className="words-count">
						{`${bodyCount} words - ${Math.ceil(bodyCount / 150)}' to read`}
					</div>
				</header>
			</div>
			<div className="outer" id="main_content_wrap">
				<section className="inner" id="main_content">
					<div className="article">
						{article.content()}
					</div>
				</section>
			</div>
			<div id="disqus_thread"/>
			<noscript>Please enable JavaScript to view the
				<a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
			</noscript>
		</>
	);
}

export default withHeader(withFooter(Article), "/blog#articles");
