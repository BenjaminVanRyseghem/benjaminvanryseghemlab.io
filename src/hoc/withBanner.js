import React, { useState } from "react";

export default function withBanner(WrappedComponent, content) {
	return () => {
		let [show, setShow] = useState(true);

		return (
			<>
				{show && <div id="resume-banner">
	                    <span className="content">
		                    {content}
	                    </span>
					<span className="close" onClick={() => setShow(false)}>×</span>
				</div>}
				<WrappedComponent/>
			</>
		);
	};
}
