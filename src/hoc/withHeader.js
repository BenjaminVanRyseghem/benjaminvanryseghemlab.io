import me from "./me.jpg";
import React from "react";

export default function withHeader(WrappedComponent, route = "/") {
	return () => (
		<>
			<div className="outer" id="header_wrap">
				<header className="inner">
					<h1 id="project_title">{"Benjamin Van Ryseghem"}</h1>
					<a href={route}><img className="avatar" src={me}/></a>
					<div className="separator left"/>
					<div className="separator middle"/>
					<div className="separator right"/>
				</header>
			</div>
			<WrappedComponent/>
		</>
	);
}
