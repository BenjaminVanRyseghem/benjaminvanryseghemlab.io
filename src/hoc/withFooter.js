import React from "react";

export default function withFooter(WrappedComponent) {
	return () => (
		<>
			<WrappedComponent/>
			<div className="outer" id="footer_wrap">
				<footer className="inner">
					Copyright © 2013-{new Date().getFullYear()} <a href="/">Benjamin Van Ryseghem</a>.<br/>
					The content of this website is distributed under the <a href="http://www.gnu.org/licenses/gpl-3.0.html" rel="noreferrer" target="_blank">GPL 3.0</a> license.
				</footer>
			</div>
		</>
	);
}
