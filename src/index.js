/* eslint-disable filenames/match-exported */
import "./stylesheets/site.css.scss";
import App from "./app";
import { AppContainer } from "react-hot-loader";
import React from "react";
import ReactDOM from "react-dom";

// Export your top level component as JSX (for static rendering)
export default App;

// Render your app
if (typeof document !== "undefined") {
	const target = document.getElementById("root");

	const renderMethod = target.hasChildNodes()
		? ReactDOM.hydrate
		: ReactDOM.render;

	const render = (Comp) => {
		renderMethod(
			<AppContainer>
				<>
					<Comp/>
				</>
			</AppContainer>,
			target
		);
	};

	// Render!
	render(App);

	// Hot Module Replacement
	if (module && module.hot) {
		module.hot.accept("./app", () => {
			render(App);
		});
	}
}
