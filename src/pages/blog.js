import "./blog.scss";
import Album from "models/album";
import AlbumsList from "components/albumsList/albumsList";
import Article from "models/article";
import ArticlesList from "components/articlesList/articlesList";
import React from "react";
import { useRouteData } from "react-static";
import withFooter from "hoc/withFooter";
import withHeader from "hoc/withHeader";

function Blog() {
	let { albums: albumsData, articles: articlesData } = useRouteData();

	const albums = albumsData
		.map((data) => new Album(data))
		.sort((one, another) => (one.date() < another.date() ? 1 : -1));

	const articles = articlesData
		.map((data) => Article.materialize(data))
		.sort((one, another) => (one.date() < another.date() ? 1 : -1));

	return (
		<div className="blog outer" id="main_content_wrap">
			<section className="inner" id="main_content">
				<p>
					On this page, you will find <a href="#articles">articles</a> about things I find interesting,
					surprising, but always
					geeky. You will also find a new <a href="#albums">music album</a> every week.
				</p>
				<section id="albums">
					<h2>Albums</h2>
					<AlbumsList albums={albums}/>
				</section>
				<section id="articles">
					<h2>Articles</h2>
					<ArticlesList articles={articles}/>
				</section>
			</section>
		</div>
	);
}

export default withHeader(withFooter(Blog));
