import React from "react";
import withFooter from "hoc/withFooter";
import withHeader from "hoc/withHeader";

class Presentations extends React.Component {
	render() {
		return (
			<div className="outer" id="main_content_wrap">
				<section className="inner" id="main_content">

					<h3>Presentations</h3>
					<p>I had the opportunity to present my work in different conferences. This page gather some of the
						materials used
						during these presentations.</p>
					<ul>
						<li>Spec Tutorial: How to do Senders/Implementors and ClassBrowser in 30 minutes, <a href="documents/presentations/SpecTuto-PharoConf13.pdf">slides</a>, done at PharoConf13,
							Bern, Switzerland (2013)
						</li>
						<li>Spec: A Framework for the Specification and Reuse of UIs and their Models, <a href="documents/presentations/Spec-IWST12.pdf" target="_blank">slides</a>, done at
							IWST12, Gand, Belgium (2012)
						</li>
						<li>Nautilus Success
							Story, <a href="documents/presentations/Nautilus_Success_Story.pdf" target="_blank">slides</a>
							(2012)
						</li>
						<li>Presentation of Hazelnut
							(<a href="http://rmod.lille.inria.fr/web/pier?_s=F2bskJjTOtRuK8TR" rel="noreferrer" target="_blank">Slides</a>)
							(<a href="https://www.youtube.com/watch?v=kfSCGYUAv4s" rel="noreferrer" target="_blank">Video</a>),
							done at Deep Into Smalltalk, Lille, France (2011)
						</li>
					</ul>

					<h3>Reports</h3>
					<p>I also had to write reports since several of the project I worked on was part of an internship.
						Here is the list
						of the reports I did:</p>
					<ul>
						<li>
							<a href="documents/presentations/Chtulhu_M1.pdf" target="_blank">Multi touch support
								implementation in Pharo</a>
							(2013)
						</li>
						<li><a href="documents/presentations/Hazelnut_L3.pdf" target="_blank">Hazelnut, dynamically
							create a kernel in a
							reflexive language</a> (2012)
						</li>
						<li><a href="documents/presentations/Hazelnut_DUT.pdf" target="_blank">Hazelnut,
							dynamically create a kernel in a
							reflexive language</a> (2011)
						</li>
					</ul>
				</section>
			</div>
		);
	}
}

export default withFooter(withHeader(Presentations));
