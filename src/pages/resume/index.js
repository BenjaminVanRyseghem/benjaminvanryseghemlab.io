/* global L */
/* eslint-disable max-lines */
import animations from "helpers/animations";
import bouldering from "./bouldering.png";
import cooking from "./cooking.png";
import ergotron from "./ergotron.png";
import getAge from "helpers/getAge";
import gitLinter from "./git-linter-big.png";
import greatThingsDone from "./great-things-done.png";
import hellboy from "./hellboy.png";
import numbro from "./numbro.png";
import pharo from "./pharo.png";
import populateCaml from "components/resume/populateCaml";
import populateCiCd from "components/resume/populateCiCd";
import populateClojure from "components/resume/populateClojure";
import populateDesignPatterns from "components/resume/populateDesignPatterns";
import populateGit from "components/resume/populateGit";
import populateGtd from "components/resume/populateGtd";
import populateHtml from "components/resume/populateHtml";
import populateJavascript from "components/resume/populateJavascript";
import populateLess from "components/resume/populateLess";
import populateLisp from "components/resume/populateLisp";
import populateOneflow from "components/resume/populateOneflow";
import populatePomodoro from "components/resume/populatePomodoro";
import populateRemote from "components/resume/populateRemote";
import populateScrum from "components/resume/populateScrum";
import populateSmalltalk from "components/resume/populateSmalltalk";
import populateSvg from "components/resume/populateSvg";
import populateUnitTests from "components/resume/populateUnitTests";
import React from "react";
import ReactDOM from "react-dom";
import sandglass from "./sandglass-big.png";
import Thermometer from "components/thermometer/thermometer";
import verticalLine from "helpers/verticalLine";
import withBanner from "hoc/withBanner";
import withFooter from "hoc/withFooter";
import withHeader from "hoc/withHeader";
import zelda from "./zelda.png";

class Resume extends React.Component {
	state = {
		forceView: false
	}

	componentDidMount() {
		let mymap = L.map("home-map").setView([49.428749, 2.8177115], 5);
		L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}", {
			tileSize: 512,
			maxZoom: 18,
			zoomOffset: -1,
			id: "mapbox/outdoors-v11",
			accessToken: "pk.eyJ1IjoiYmVuamFtaW52YW5yeXNlZ2hlbSIsImEiOiJjajNic2hlcnEwMDVtMndxa3ZnOXo1eDNmIn0.5MSBKKnObMU89RD3vfKx3Q"
		}).addTo(mymap);

		L.marker([49.428749, 2.8177115]).addTo(mymap);
		animations();
		verticalLine();
	}

	thermometer(options) {
		return (
			<Thermometer
				{...options}
			/>
		);
	}

	renderImageTag(src) {
		return (
			<img src={src}/>
		);
	}

	separatedTitle(title) {
		return (
			<div className="separated-title">
				<div className="separator left"/>
				{title}
				<div className="separator right"/>
			</div>
		);
	}

	renderDescriptionContent(description) {
		if (description.name) {
			return (
				<>
					<span className="name">{description.name}</span>
					<div className="description">{description.description}</div>
				</>
			);
		}
		return (
			description.description
		);
	}

	renderDescription(description) {
		if (description.length < 2) {
			return <div>{this.renderDescriptionContent(description[0])}</div>;
		}

		return (<ul className="tasks-description">
			{description.map((each, index) => <li key={index}>{this.renderDescriptionContent(each)}</li>)}
		</ul>);
	}

	timelineItem({type, title, company, mission, description, date}) {
		return (
			<div className="timeline-block">
				<div className={`timeline-img ${type}`}><i/></div>
				<div className="timeline-content">
					<h2 className="timeline-title">
						<span className="title">{title}</span>
						<span className="company">{company}</span>
					</h2>
					<div className="mission">{mission}</div>
					{this.renderDescription(description)}
					<span className="date">{date}</span>
				</div>
			</div>
		);
	}

	timelineJob(options) {
		return this.timelineItem({
			...options,
			type: "job"
		});
	}

	timelineEducation(options) {
		return this.timelineItem({
			...options,
			type: "education",
			description: [options.description]
		});
	}

	timelineProject(options) {
		return this.timelineItem({
			...options,
			type: "project"
		});
	}

	renderLink(name, to) {
		return <a href={to}> {name} </a>;
	}

	showSplash() {
		return (
			<div className="splash">
				<div className="content">
					<h3>Too small screen</h3>
					<div>This page is designed for screens bigger than 1080x676.</div>
					<div>You can see
						the <a href="/documents/cv/Resume.pdf"> {"PDF version"} </a>
						of my resume, or <a className="continue" onClick={(event) => {
							event.preventDefault();
							this.setState({forceView: true});
						}}>continue</a> at you own risks.
					</div>
				</div>
			</div>
		);
	}

	render() {
		let width = global.innerWidth;
		let height = global.innerHeight;
		let shouldShowSplash = width < 1080 || height < 676;

		return (
			<div className="outer resume" id="main_content_wrap">
				<div className="inner" id="main_content">
					{
						shouldShowSplash &&
						!this.state.forceView &&
						ReactDOM.createPortal(this.showSplash(), document.body)
					}
					<section id="presentation">
						<div className="info-content full">
							<span className="description">I am a {getAge()} years old software development enthusiast and I have the chance to live from my passion. I spent the last 10 years improving my skills, learning new things and working on awesome projects.</span>
						</div>

						<div className="info-content left">
							<div className="description">
								<p>Margny-lès-Compiègne is a nice small town surrounded by a wonderful forest, and very
									close to Paris.</p>
								<p>As I love living here, I will only consider remote positions.</p>
							</div>
							<div className="map">
								<div className="image" id="home-map"/>
								<p>Map data &copy;
									<a href="https://openstreetmap.org">OpenStreetMap</a> contributors,
									<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>
								</p>
								<p>Imagery © <a href="https://mapbox.com">Mapbox</a></p>
							</div>
						</div>

						<div className="quote-container beck">
							<div className="quote">
								<div>{"I'm not a great programmer;"}</div>
								<div>{"I'm just a good programmer with great habits."}</div>
							</div>
							<div className="author">Kent Beck</div>
						</div>
					</section>
					<section id="timeline">
						{this.separatedTitle("Timeline")}
						<div className="timeline-container">
							<span className="timeline-arrow"/>
							{this.timelineJob({
								title: "Lead UI engineer",
								mission: "Accounting solution",
								company: "finsit",
								date: "January 2023 - Today",
								description: [
									{
										name: "Design system",
										description: "Implementation of UI components extracted from our code-base to be able to integrate the group design-system, using today's standards technologies as CSS3 and web-components."
									},
									{
										name: "Front-end development",
										description: "Improvement of the current architecture and implementation of new features."
									},
									{
										name: "Finsit Conference",
										description: "Implementation of an in-house conference with both lectures and workshops to share knowledge among us, and provide an in-detail view of new technologies or new architectures we recently introduced."
									},
									{
										name: "UI Team",
										description: "Creation of a new UI team, defining new processes, new tools, and helping juniors to onboard."
									}
								]
							})}
							{this.timelineJob({
								title: "Senior software engineer",
								mission: "Accounting solution",
								company: "finsit",
								date: "May 2021 - December 2022",
								description: [
									{
										name: "Front-end development",
										description: "Join the effort to bring new features in an easy-to-grasp way, while focusing on providing the best software architecture possible."
									},
									{
										name: "Juniors tutoring",
										description: "Experimentation and implementation of processes to help juniors grow within finsit, both at a technical level and at a culture level."
									},
									{
										name: "Design system",
										description: "Extraction of an in-house design-system to help developers building user interfaces faster and in a more consistent way. First step to integrate the group design-system."
									}
								]
							})}
							{this.timelineJob({
								title: "Lead full stack engineer",
								mission: "Cybersecurity apps",
								company: "Cyberzen",
								date: "March 2019 - April 2021",
								description: [
									{
										name: "Full stack development",
										description: "Development of in-house applications to audit companies security policies built upon a full Javascript stack. We use Express server-side, and React front-side to build modern applications."
									},
									{
										name: "Digital transition",
										description: "Implementation of tools to help companies to transition smoothly into the numeric world. We design solutions to accelerate companies growth and to convert their activities to use today's technologies and capabilities."
									},
									{
										name: "Cybersecurity Android app",
										description: "Development of an Android app using Kotlin to scan credit card and display informations stored on a contactless payment card."
									},
									{
										name: "Remote",
										description: "Setup of a remote-friendly environment as the first remote-employee of the company."
									}
								]
							})}
							{this.timelineJob({
								title: "Senior front-end engineer",
								mission: "Seated ticketing solution",
								company: "Weezevent",
								date: "October 2017 - February 2019",
								description: [
									{
										name: "Seated ticketing",
										description: "Development of a seated ticketing solution in multiple layers from raw map drawing using D3 to a complete sales workflow for final users. Attention was given to the design to allow multiple rendering solutions (a in-house solution in 2d as well as PACIFA for 3d rendering), but also to allow several parts of the application to plug onto the solution to add their own interactions."
									},
									{
										name: "Front-end development",
										description: "In charge of improving the overall javascript code and practices as well as bringing the latest of the front-end ecosystem to improve the quality of the code and the skills of other devs when it comes to Javascript."
									},
									{
										name: "Remote",
										description: "Setup of a remote-friendly environment as the first remote-employee of the company. Teaching of good-practices experienced over years good communication process, async meeting, useful scripts, etc."
									}
								]
							})}
							{this.timelineJob({
								title: "Software Engineer",
								mission: "Full-stack engineer, UX & UI",
								company: "Företagsplatsen",
								date: "July 2014 - June 2017",
								description: [
									{
										name: "Front-end development",
										description: "JavaScript development (ES5/ES6), client-side architecture, UX & UI design (Less/CSS, SVG icons). Unit testing is an important part of the development process, we are using Jasmine for JavaScript testing (through Karma)."
									},
									{
										name: "Backend development",
										description: "Backend in C#, using the MVP framework for REST API handling. The server fetches the data from a CouchDB database. We use NUnit on the server-side for unit testing."
									},
									{
										name: "CI & DevOps",
										description: "Experience with automated testing via TeamCity. Automatic deployment on Microsoft Azure using Ansible."
									}
								]
							})}

							{this.timelineProject({
								title: "Research paper",
								mission: "First Author",
								company: "Science of Computer Programming",
								date: "December 2014",
								description: [{description: "Publication of the article \"Seamless Composition and Reuse of Customizable User Interfaces with Spec\" by Benjamin Van Ryseghem, Stéphane Ducasse, Johan Fabry, in Science of Computer Programming Volume 96, Part 1"}]
							})}

							{this.timelineJob({
								title: "Scientific Engineer",
								mission: "Redesign of the Pharo Smalltalk IDE",
								company: "Inria (RMoD)",
								date: "July - August 2013",
								description: [
									{description: "Responsible for the refactoring of the legacy \"Morphic UI\" framework."},
									{description: "Implementation of Spec a UI-generation framework."},
									{description: "Development of a fully-featured IDE solution for Pharo."}
								]
							})}

							{this.timelineJob({
								title: "Software engineer",
								mission: "Consulting",
								company: "Företagsplatsen",
								date: "June 2013 - June 2014",
								description: [
									{description: "Development of a new major release of the web application technical migration from a template-based server-side application to a component-based JavaScript single-page application."},
									{description: "Development of a cloud-based document archive application à la Dropbox for accounting agencies."}
								]
							})}

							{this.timelineJob({
								title: "Software engineer",
								mission: "Google Summer Of Code",
								company: "Google",
								date: "June - August 2013",
								description: [{description: "Improvement of Spec decoupling the models from the UI framework for better extensibility."}]
							})}

							{this.timelineJob({
								title: "Young Engineer",
								mission: "UI framework development",
								company: "Inria (RMoD)",
								date: "March 2013",
								description: [
									{description: "Implementation of new UI widgets (in Morphic)."},
									{description: "Refactoring of the legacy UI codebase."}
								]
							})}

							{this.timelineEducation({
								title: "Computer Science Master Degree",
								description: "First Year, UI/UX speciality",
								date: "2012 - 2013"
							})}

							{this.timelineProject({
								title: "Facilitator",
								mission: "Head of the Student volunteer program",
								date: "August 2012",
								company: "ESUG",
								description: [{description: "Coordination of the students and the well-being of the attendees at the 20th International Smalltalk Conference."}]
							})}

							{this.timelineProject({
								title: "Research paper",
								mission: "First Author",
								date: "August 2012",
								company: "IWST '12",
								description: [{description: "Publication of the article \"A Framework for the Specification and Reuse of UIs and their Models\" by Benjamin Van Ryseghem, Stéphane Ducasse, and Johan Fabry at IWST '12."}]
							})}

							{this.timelineJob({
								title: "Young Engineer",
								mission: "UI framework development",
								company: "Inria (RMoD)",
								date: "July - August 2012",
								description: [{description: "Improvement of widgets and the development of a widgets generation framework."}]
							})}

							{this.timelineJob({
								title: "Software engineer",
								mission: "Google Summer Of Code",
								company: "Google",
								date: "June - August 2012",
								description: [{description: "Implementation the Traits support in Nautilus, the new default Pharo IDE I previously developed."}]
							})}

							{this.timelineEducation({
								title: "Bachelor Degree in Computer Science",
								description: "Speciality in Software Design and UI",
								date: "2009 - 2012"
							})}
						</div>
					</section>
					<section id="highlights">
						{this.separatedTitle("Highlights")}
						<div>Some highlights of FLOSS projects I developed or to which I was a
							core contributor
						</div>
						<ul className="floss">
							<li>
								<span className="logo">{this.renderImageTag(numbro)}</span>
								<div className="description">As the author of
									<span
										className="important">{this.renderLink("numbro", "https://numbrojs.com//")}</span>
									I had the opportunity to collaborate with a great variety of people
									and to provide a formatting solution that it downloaded more than 13 million times a
									year,
									supporting more than 60 cultures. It is used mainly by companies and embedded in
									multiple
									frameworks as the default tool for currency formatting.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(pharo)}</span>
								<div className="description">As a core maintainer of
									<span className="important">{this.renderLink("Pharo", "http://pharo.org/")}</span>
									(an open source
									Smalltalk implementation), most of my Smalltalk projects have been
									integrated in the distribution. My other Smalltalk projects can be
									found on Smalltalkhub. Among those projects are the complete IDE
									solution,
									the main UI generation framework, a VCS solution, and a couple of
									widgets (tabs, circular menu, lists, etc.)
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(gitLinter)}</span>
								<div className="description">
									<span
										className="important">{this.renderLink("git-linter", "https://gitlab.com/BenjaminVanRyseghem/git-linter")}</span>
									is a node-based command line tool and a docker-based GitHub/Gitlab
									CI
									integration that lint git commit messages using project-defined
									rules <em>à la</em> eslint.
									The code is distributed under the GPL 3.0 license.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(sandglass)}</span>
								<div className="description">
									<span
										className="important">{this.renderLink("SandGlass", "https://gitlab.com/BenjaminVanRyseghem/SandGlass")}</span>
									is an electron-based systray app used to track my working
									time. It provides a CLI for workflow integration and D3 based
									histograms of time per projects. The code is distributed under the
									GPL 3.0 license.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(greatThingsDone)}</span>
								<div className="description">
									<span
										className="important">{this.renderLink("Great Things Done", "https://gitlab.com/BenjaminVanRyseghem/great-things-done")}</span>
									is a keyboard-centric GTD application based on
									Electron, and implemented in Clojure/ClojureScript. The front-end
									uses Reagent (an adaptor to React in CLJS), and implements some
									interesting features like a fully encrypted file-based database,
									global shortcut with macOS integrations, or Dock icon support. The
									code is distributed under the EPL 1.0 license.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(ergotron)}</span>
								<div className="description">
									<span
										className="important">{this.renderLink("ergotron", "https://gitlab.com/BenjaminVanRyseghem/ergotron-firmware")}</span>
									is a hand-made custom keyboard I built from scratch. The firmware is
									based on the excellent ergodox-firmware by Ben Blazak. The firmware
									has been extended to support a lot more keys and LEDs, leading to
									a new hardware layout. The code is distributed under the MIT
									license.
								</div>
							</li>
						</ul>
					</section>
					<section id="hobbies">
						{this.separatedTitle("Hobbies")}
						<div>A small glimpse at things I love to do during my spare time</div>
						<ul className="hobbies">
							<li>
								<span className="logo">{this.renderImageTag(cooking)}</span>
								<div className="description">
									<span className="important">Cooking</span> is where I spend most time
									out of work.
									I love to prepare a nice meal for my friends and I.
									I am always improving my skills by experimenting new recipes and
									cuisines.
									The fact that it requires to be focused and precise is a great way
									to free your mind.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(bouldering)}</span>
								<div className="description">
									<span className="important">Bouldering</span> is a very fun sport with
									an awesome community.
									Even though it is an individual sport, you are never alone, and
									people always offer hints and tips.
									It requires to always outperform yourself and be very focus which I
									find are providing great satisfaction.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(zelda)}</span>
								<div className="description">
									<span className="important">Gaming</span> is most probably the hobby I
									practise for the longest time.
									It is really enjoyable to dive in a completely new universe, with
									whole new set of rules to learn, and see how your past experiences
									can help you
									mastering this new challenge. This apply to video games but also to
									board games or role playing games.
									And beside being fun, playing has always be a great way to learn and
									experiment.
								</div>
							</li>
							<li>
								<span className="logo">{this.renderImageTag(hellboy)}</span>
								<div className="description">
									<span className="important">Miniatures painting</span> is my new hobby, and probably
									the one I wanted to dive in for the longest time. I was always impressed by those
									showcases we can see in game shops, but never tried. When I got my 3d printer, I
									decided it was time to give it a try, and a couple of years later with 100+ mini
									painted I can definitely say I am hooked. It is quite magical to play boardgames
									with friends using only mini you spent hours painting.
								</div>
							</li>
						</ul>
					</section>
					<section id="job">
						{this.separatedTitle("My dream job")}
						<div className="content">
							<div className="item">
								<div className="title">Great people to work with</div>
								<div className="description">
									We learn from our pairs and spend several
									hours a day with them. So having a good team spirit is mandatory.
									And a good team spirit comes from good team members.
								</div>
							</div>
							<div className="item">
								<div className="title">Not just another brick in the wall</div>
								<div className="description">
									Everyone should matter and have the
									opportunity to impact on what they do. I am also strongly convinced
									that involved worker are more motivated and thus more productive.
								</div>
							</div>
						</div>
						<div className="content">
							<div className="item">
								<div className="title">Self-Organized</div>
								<div className="description">
									I strongly believe in self-organized people as well as in
									self-organized teams.
									Decisions taken by doers are a lot more motivating and reflects
									better the team capabilities.
									Even though a team my need a leader occasionally, team members
									should be trusted to find the best way
									to reach the defined goals.
								</div>
							</div>
							<div className="item">
								<div className="title">Remote</div>
								<div className="description">
									Remote positions allow to gather best suited people together.
									Not only the ones that happen to be located close by.
									Also mixing cultures brings creativity and opens minds.
								</div>
							</div>
						</div>
					</section>
					<section id="skills">
						{this.separatedTitle(<div className="skill-title"><span>Skills</span>
							<span className="play-button" onClick={async (event) => {
								let hasScrolled = false;
								let top = document.getElementById("more").getBoundingClientRect().top + window.scrollY - window.innerHeight + 200;
								let listener = () => {
									hasScrolled = true;
								}
								window.addEventListener("wheel", listener);


								for (let y = window.scrollY; y <= top; y += 80) {
									window.scrollTo({top: y, behavior: 'smooth'})
									await this.scrollDelay(200);
									if (hasScrolled) {
										window.removeEventListener("wheel", listener);
										return;
									}
								}
							}}>
								<span className="fa fa-play-circle"/>
							</span>
						</div>)}

						<span id="scroll-trigger"/>
						<div id="skills-move">
							{this.thermometer({
								title: "OOP",
								percent: 95,
								id: "oop",
								details: {
									st: {
										value: 30,
										name: "Smalltalk",
										popup: populateSmalltalk()
									},
									js: {
										value: 25,
										name: "JavaScript",
										popup: populateJavascript()
									},
									dp: {
										value: 40,
										name: "Design Patterns",
										popup: populateDesignPatterns()
									}
								}
							})}
							{this.thermometer({
								title: "FP",
								percent: 70,
								id: "fp",
								details: {
									lisp: {
										value: 30,
										name: "Lisp",
										popup: populateLisp()
									},
									clojure: {
										value: 35,
										name: "Clojure",
										popup: populateClojure()
									},
									caml: {
										value: 5,
										name: "Caml",
										popup: populateCaml()
									}
								}
							})}
							{this.thermometer({
								title: "Web",
								percent: 85,
								id: "web",
								details: {
									html: {
										value: 40,
										name: "HTML5 / CSS3",
										popup: populateHtml()
									},
									"less-scss": {
										value: 25,
										name: "Less / SCSS",
										popup: populateLess()
									},
									svg: {
										value: 20,
										name: "Icons",
										popup: populateSvg()
									}
								}
							})}
							{this.thermometer({
								title: "Agile",
								percent: 90,
								id: "agile",
								details: {
									"unit-tests": {
										value: 40,
										name: "Unit tests",
										popup: populateUnitTests()
									},
									scrum: {
										value: 30,
										name: "Scrum / Kanban",
										popup: populateScrum()
									},
									pomodoro: {
										value: 10,
										name: "Pomodoro",
										popup: populatePomodoro()
									},
									oneflow: {
										value: 10,
										name: "OneFlow",
										popup: populateOneflow()
									}
								}
							})}
							{this.thermometer({
								title: "Tools",
								percent: 80,
								id: "tools",
								details: {
									remote: {
										value: 24,
										name: "Remote",
										popup: populateRemote()
									},
									git: {
										value: 24,
										name: "Git",
										popup: populateGit()
									},
									gtd: {
										value: 19,
										name: "GTD",
										popup: populateGtd()
									},
									tasks: {
										value: 13,
										name: "CI / CD",
										popup: populateCiCd()
									}
								}
							})}
						</div>
					</section>
					<section id="more">
						{this.separatedTitle("More")}
						<div className="content">
							<ul>
								<li>
									<i className="fa fa-at"/>{this.renderLink("Send me an email", "mailtobenjamin@vanryseghem.com")}
								</li>
								<li>
									<i className="fa fa-github"/>{this.renderLink("Visit my GitHub page", "//github.com/BenjaminVanRyseghem")}
								</li>
								<li>
									<i className="fa fa-gitlab"/>{this.renderLink("Visit my GitLab page", "//gitlab.com/BenjaminVanRyseghem")}
								</li>
								<li>
									<i className="fa fa-home"/>{this.renderLink("Visit my webpage", "//benjamin.vanryseghem.com")}
								</li>
								<li>
									<i className="fa fa-file-pdf-o"/>{this.renderLink("Download the resume", "/documents/cv/Resume.pdf")}
								</li>
							</ul>
						</div>
					</section>

					<button id="scroll-to-top" title="Scroll to top" onClick={() => {
						global.scrollTo({
							top: 0,
							behavior: "smooth"
						});
					}}>
			<span className="fa-stack fa-lg">
  			<i className="fa fa-circle-thin fa-stack-2x"/>
  			<i className="fa fa-angle-double-up fa-stack-1x"/>
			</span>
					</button>
				</div>
			</div>
		);
	}

	scrollDelay(ms) {
		return new Promise(res => setTimeout(res, ms));
	}
}

export default withBanner(
	withFooter(withHeader(Resume)),
	<p>In a hurry? Directly see the <a href="/documents/cv/Resume.pdf">PDF version</a></p>
);
