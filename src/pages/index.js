/* eslint-disable filenames/match-exported */
import getAge from "helpers/getAge";
import React from "react";
import withFooter from "hoc/withFooter";
import withHeader from "hoc/withHeader";

class Index extends React.Component {
	render() {
		return (
			<div className="outer" id="main_content_wrap">
				<div className="inner" id="main_content">
					<section>
						<p>I am a {getAge()} years old software development
							enthusiast and I have the chance to live from my passion. I spent the
							last 10 years improving my skills, learning new things and working on
							awesome projects. I am a former Smalltalker, now having fun with
							functional languages
							(especially <a href={"https://clojure.org/"}>{"Clojure"}</a>
							/<a href={"https://github.com/clojure/clojurescript"}>{"ClojureScript"}</a>). I live
							in <a href={"https://www.mairie-margnylescompiegne.fr/"}>{"Margny-lès-Compiègne"}</a>,
							France.
						</p>

						<p>You can follow me on <a href={"https://github.com/BenjaminVanRyseghem"}>{"GitHub"}</a>,
							on <a href={"https://gitlab.com/BenjaminVanRyseghem"}>{"GitLab"}</a>, or contact me
							by <a href={"mailto:benjamin@vanryseghem.com"}>{"email."}</a>
						</p>

						<p>
							If you want more information, please visit
							my <a href={"resume"}>{"resume"}</a>.
						</p>
					</section>
					<section>
						<h3>Blog</h3>

						<p>I sometimes write geeky <a href={"blog"}>{"articles"}</a> about things I like, I did, or just
							find
							interesting.
						</p>
					</section>

					<section>
						<h3>Projects</h3>

						<p>Here is a handful of my projects:</p>
						<ul>
							<li>
								<a href={"https://numbrojs.com"}>{"numbro"}</a>: a JS framework for number formatting
								(inspired by momentjs)
							</li>
							<li>
								<a href={"projects/sandglass"}>{"SandGlass"}</a>: a time-tracker app based on Electron
							</li>
							<li>
								<a href={"projects/spec"}>{"Spec"}</a>: a framework for the specification and generation
								of user interfaces for Pharo
							</li>
						</ul>
					</section>
					<section>
						<h3>Presentations</h3>

						<p>I had the opportunity to do some <a href={"presentations"}>{"presentations"}</a> of my work
							in
							different
							conferences.</p>
					</section>
					<section>
						<h3>Publications</h3>

						<p>Along the way, I had the opportunity to publish papers:</p>
						<ul>
							<li>
								<a href={"documents/publications/Spec-Elsevier-Seamless_composition_and_reuse_of_customizable_user_interfaces_with_Spec.pdf"}>{"Seamless Composition and Reuse of Customizable User Interfaces with Spec"}</a>
								, Benjamin Van Ryseghem, Stéphane Ducasse, Johan
								Fabry, <a href={"https://www.sciencedirect.com/science/article/pii/S0167642313003286"}>{"Science of Computer Programming Volume 96, Part 1 (December 2014)"}</a>
							</li>
							<li>
								<a href={"documents/publications/Spec-IWST12-Final.pdf"}>{"Spec: A Framework for the Specification and Reuse of UIs and their Models"}</a>
								, Benjamin Van Ryseghem, Stéphane Ducasse, Johan Fabry, Proceedings of ESUG
								International
								Workshop on
								Smalltalk Technologies (2012)
							</li>
							<li><a href={"documents/publications/SpecTechReport.pdf"}>{"Spec — Technical Report"}</a>,
								Benjamin Van Ryseghem (2012)
							</li>
							<li>
								<a href={"documents/publications/BootstrappingASmalltalk.pdf"}>{"Bootstrapping a Smalltalk"}</a>,
								Gwenaël Casaccio, Stéphane Ducasse, Luc Fabresse, Jean-Baptiste Arnaud,
								Benjamin Van Ryseghem, Smalltalks (2011)
							</li>
						</ul>
					</section>
					<section>
						<h3>Smalltalk</h3>
						I was a <a href={"https://pharo.org"}>{"Pharo"}</a> core maintainer, in charge of the IDE and
						the
						widgets
						framework Morphic.
						<p>I am the author of <a href={"projects/spec"}>{"Spec"}</a>, a new framework for the
							specification
							and
							generation
							of user interfaces, and Nautilus, a new code-browser for Pharo.</p>
					</section>
				</div>
			</div>
		);
	}
}

export default withFooter(withHeader(Index));
