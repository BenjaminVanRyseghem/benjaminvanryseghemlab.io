import AbstractBlogEntry from "./abstractBlogEntry";
import dashify from "dashify";

const album = Symbol("album");
const artist = Symbol("artist");
const song = Symbol("song");
const url = Symbol("url");
const description = Symbol("description");
const discovered = Symbol("discovered");

export default class Album extends AbstractBlogEntry {
	constructor({
		meta: {
			album: albumData,
			artist: artistData,
			date: dateData,
			song: songData,
			url: urlData,
			description: descriptionData,
			discovered: discoveredData
		}
	}) {
		super(...arguments); // eslint-disable-line prefer-rest-params
		this[album] = albumData;
		this[artist] = artistData;
		this[song] = songData;
		this[url] = urlData;
		this[description] = descriptionData;
		this[discovered] = discoveredData;
	}

	id() { return dashify(this[artist]); }

	category() { return this.momentDate().format("YYYY"); }

	album() { return this[album]; }

	artist() { return this[artist]; }

	song() { return this[song]; }

	url() { return this[url]; }

	description() { return this[description]; }

	discovered() { return this[discovered]; }

	weekNumber() {
		return this.momentDate()
			.add(1, "day")
			.format("W");
	}

	urlSegment() { return `week-${this.weekNumber()}`; }
}
