import AbstractBlogEntry from "./abstractBlogEntry";
import dashify from "dashify";
import { deserialize } from "react-serialize";

const title = Symbol("title");
const tagline = Symbol("tagline");
const image = Symbol("image");
const category = Symbol("category");
const content = Symbol("content");

export default class Article extends AbstractBlogEntry {
	constructor({
		meta: {
			title: titleData,
			date: dateData,
			tagline: taglineData,
			image: imageData,
			category: categoryData
		}, content: contentData = ""
	} = {}) {
		super(...arguments); // eslint-disable-line prefer-rest-params

		this[title] = titleData;
		this[tagline] = taglineData;
		this[image] = imageData;
		this[category] = categoryData;
		this[content] = contentData;
	}

	id() { return `${this.formattedDate()}-${dashify(this[title])}`; }

	url() {
		return `/blog/${this.category()}/${this.urlSegment()}`;
	}

	tagline() { return this[tagline]; }

	content() { return this[content]; }

	title() { return this[title]; }

	image() { return this[image]; }

	category() { return this[category]; }

	urlSegment() { return dashify(this[title]); }

	findParagraphIn(node) {
		if (node.type === "p") {
			return node;
		}

		if (!node.props || !node.props.children) {
			return null;
		}

		let { children } = node.props;

		for (let i = 0; i < children.length; i++) {
			let found = this.findParagraphIn(children[i]);
			if (found) {
				return found;
			}
		}

		return null;
	}

	summary() {
		if (Array.isArray(this[content])) {
			for (let i = 0; i < this[content].length; i++) {
				let found = this.findParagraphIn(this[content][i]);
				if (found) {
					return found;
				}
			}
			return null;
		}

		return this.findParagraphIn(this[content]);
	}

	countWordsIn(node) {
		if (typeof node === "string") {
			return node.split(/\s/).length;
		}

		if (!node.props || !node.props.children) {
			return 0;
		}

		return this.countWords(node.props.children);
	}

	countWords(nodeOrArray) {
		if (Array.isArray(nodeOrArray)) {
			return nodeOrArray.reduce((acc, curr) => acc + this.countWordsIn(curr), 0);
		}

		return this.countWordsIn(nodeOrArray);
	}

	bodyCount() {
		return this.countWords(this[content]);
	}

	static materialize(data) {
		let { content: serializedReact } = data;
		let deserializedContent = serializedReact ? deserialize(serializedReact) : "";
		return new this({
			...data,
			content: deserializedContent
		});
	}
}
