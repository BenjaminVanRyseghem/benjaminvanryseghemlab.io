import moment from "moment";
import { serialize } from "react-serialize";

const date = Symbol("date");
const isDraft = Symbol("isDraft");
const raw = Symbol("raw");

export default class AbstractBlogEntry {
	constructor(options) {
		let {
			meta: {
				date: dateData,
				draft
			}
		} = options;
		this[raw] = options;
		this[date] = moment(dateData, "YYYY-MM-DD");
		this[isDraft] = !!draft;
	}

	isDraft() { return this[isDraft]; }

	date() { return this[date].toDate(); }

	momentDate() { return this[date].clone(); }

	isAfter(time) { return this[date].isAfter(time); }

	formattedDate() { return this[date].format("YYYY-MM-DD"); }

	toRaw() {
		return {
			...this[raw],
			content: serialize(this[raw].content)
		};
	}

	category() {
		throw new Error("[AbstractBlogEntry/urlSegment] Should be overriden");
	}

	urlSegment() {
		throw new Error("[AbstractBlogEntry/urlSegment] Should be overriden");
	}
}
