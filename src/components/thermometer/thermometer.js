import drawThermometerDetails from "helpers/drawThermometerDetails";
import PropTypes from "prop-types";
import React from "react";

export default class Thermometer extends React.Component {
	static defaultProps = {};

	static propTypes = {
		details: PropTypes.object.isRequired,
		id: PropTypes.string.isRequired,
		percent: PropTypes.number.isRequired,
		title: PropTypes.string.isRequired
	};

	state = {};

	componentDidMount() {
		drawThermometerDetails(this.props.id, this.props.details);
	}

	render() {
		let { percent, id, title } = this.props;
		return (
			<div className="thermometer-wrapper" data-percent={percent} id={id}>
				<div className="thermometer-content">
					<div className={`thermometer p${percent}`}>
						<span className="percent">{`${percent}%`}</span>
						<span className="slice-container">
                            <svg className="slice">
								<path className="bar" d={`M 70 4 A 66 66 1 1 1 ${70 + (66 * Math.cos((percent * Math.PI / 50) - Math.PI / 2))} ${70 + (66 * Math.sin((percent * Math.PI / 50) - Math.PI / 2))}`} stroke="red" strokeWidth="4"/>
                            </svg>
                        </span>
						<div className="details" id={`container-${id}`}/>
					</div>
					<div className="title">{title}</div>
				</div>
			</div>
		);
	}
}
