import LightBox from "components/ligthbox/lightBox";
import { Link } from "@reach/router";
import moment from "moment";
import PropTypes from "prop-types";
import React from "react";

export default class ArticlesList extends React.Component {
	static propTypes = { articles: PropTypes.array.isRequired };

	renderArticle(article) {
		if (article.isAfter(new Date())) {
			return null;
		}

		let bodyCount = article.bodyCount();
		return (
			<li key={article.title()} className="blog-article album">
				<div className="clearfix" id={article.title()}>
					<h3>
						<Link to={article.url()}>{article.title()}</Link>
						<div className="words-count">
							{`${bodyCount} words - ${Math.ceil(bodyCount / 150)}' to read`}
						</div>
					</h3>
					<div className="post post-date">{moment(article.date()).format("MMMM DD, YYYY")}</div>
				</div>
				<div className="article-image">
					<LightBox
						image={`/images/post/${article.image()}`}
						title={article.title()}
					/>
				</div>
				<div className="post-excerpt">
					{article.summary()}
				</div>
				<div className="more">
					<Link to={article.url()}>Read more</Link>
				</div>
			</li>
		);
	}

	render() {
		return (
			<ul className="articles">
				{this.props.articles.sort().map((article) => this.renderArticle(article))}
			</ul>
		);
	}
}
