import React from "react";

/**
 * @module populateDesignPatterns
 */
export default function populateDesignPatterns() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide crockford">
					<div className="slide-quote">
						<div className="quote">
							You should know when to use them, but more important when
							<span className="important"> not </span>
							to use them
						</div>
						<div className="author">
							<span className="name">My design teacher</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Shared ground knowledge</li>
						<li>Scientific publication about a design pattern</li>
						<li>{"Know by heart \"Design Patterns\""}</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>The GoF members are Smalltalkers</li>
						<li>Design patters are paradigm specific, not language specific</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
