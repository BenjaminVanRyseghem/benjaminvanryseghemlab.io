import React from "react";

/**
 * @module populateClojure
 */
export default function populateClojure() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide hickey">
					<div className="slide-quote">
						<div className="quote">
							{"Yields the unevaluated form. See \"Special Forms\" for more information."}
						</div>
						<div className="author">
							<span className="name">Rick Hickey</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Many side projects in Clojure/ClojureScript</li>
						<li>Contributed to LightTable</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>A modern Lisp</li>
						<li>Runs on top of Java, so virtually everywhere</li>
						<li>Immutability makes things a lot simpler</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
