import React from "react";

/**
 * @module populateSmalltalk
 */
export default function populateSmalltalk() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide reeves">
					<div className="slide-quote">
						<div className="quote">I know OOP!</div>
						<div className="author">
							<span className="name">Keanu Reeves</span>
							<div className="annotation">
								<span>(after learning </span>
								<span className="important">Smalltalk</span>
								<span>)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Main contributor of Pharo for 3 years</li>
						<li>Work in the research team in charge of Pharo</li>
						<li>Rewrite of the IDE tools-set</li>
						<li>Research paper publications</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Fully object-oriented</li>
						<li>Smalltalk invented everything</li>
						<li>Proven most productive general-purpose language</li>
						<li>Make you <span className="important">really</span> understand what an
							object is
						</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
