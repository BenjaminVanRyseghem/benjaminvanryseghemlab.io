import gitLinter from "./git-linter.png";
import ranko from "./ranko.png";
import React from "react";
import sandglass from "./sandglass.png";
import teamwall from "./teamwall.png";
import ysos from "./ysos.png";

/**
 * @module populatePomodoro
 */
export default function populatePomodoro() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide cirillo">
					<div className="slide-quote">
						<div className="quote">
							Work with time - Not against it
						</div>
						<div className="author">
							<span className="name">Francesco Cirillo</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Adept of this technique when a task require a lot of focus</li>
						<li>This is done using the Pomodoro Technique®</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>{"Help to find \"the flow\""}</li>
						<li>Improve pair-programming sessions</li>
						<li>Easy to set</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
