import React from "react";

/**
 * @module populateOneflow
 */
export default function populateOneflow() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide ruskin">
					<div className="slide-quote">
						<div className="quote">
							OneFlow is a simpler alternative to GitFlow
						</div>
						<div className="author">
							<span className="name">Adam Ruka</span>
							<div className="annotation">
								<span>(<a href="http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow">End of Line</a>)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Enforce a simple git flow in teams</li>
						<li>Write tooling support for CI/CD</li>
						<li>Write git helpers for flow support</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>A good git history is mandatory</li>
						<li>Shared workflow eases everyone daily tasks</li>
						<li>Code review should also enforce good git history</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
