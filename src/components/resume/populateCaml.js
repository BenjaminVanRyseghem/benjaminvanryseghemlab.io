import React from "react";

/**
 * @module populateCaml
 */
export default function populateCaml() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide">
					<ul>
						<li>First language learned</li>
						<li>Learned all the language features</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Nice syntax</li>
						<li>Many rare language features</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
