import React from "react";

/**
 * @module populateGtd
 */
export default function populateGtd() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide allen">
					<div className="slide-quote">
						<div className="quote">
							There are no problems, only projects
						</div>
						<div className="author">
							<span className="name">David Allen</span>
							<div className="annotation">
								<span>{"Creator of \"Getting Things Done\""}</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>GTD enthusiast for years</li>
						<li>Empty my inbox every morning</li>
						<li>Multiple side-projects revolving around GTD</li>
					</ul>
				</section>
				<section className="slide dante">
					<div className="slide-quote">
						<div className="quote">
							The secret of getting things done is to act
						</div>
						<div className="author">
							<span className="name">Dante Alighieri</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Free your mind of burdens</li>
						<li>Trick your brain into doing things</li>
						<li>Say goodbye to procrastination</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
