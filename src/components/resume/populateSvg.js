import gitLinter from "./git-linter.png";
import ranko from "./ranko.png";
import React from "react";
import sandglass from "./sandglass.png";
import teamwall from "./teamwall.png";
import ysos from "./ysos.png";

/**
 * @module populateSvg
 */
export default function populateSvg() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide icons">
					<div className="slide-quote">
						<div className="icons">
							<img alt="ysos" src={ysos}/>
							<img alt="ranko" src={ranko}/>
							<img alt="sandglass" src={sandglass}/>
							<img alt="teamwall" src={teamwall}/>
							<img alt="gitLinter" src={gitLinter}/>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Design of SVG icons</li>
						<li>Design of pixel perfect icons</li>
						<li>Embedding of SVG in HTML</li>
						<li>Styling of SVG via CSS</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Icons are a big part of a design</li>
						<li>SVG scales</li>
						<li>{"FontAwesome can't fit with everything"}</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
