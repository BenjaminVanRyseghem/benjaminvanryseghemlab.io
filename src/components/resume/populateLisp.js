import React from "react";

/**
 * @module populateLisp
 */
export default function populateLisp() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide key">
					<div className="slide-quote">
						<div className="quote">
							The greatest single programming language ever designed
						</div>
						<div className="author">
							<span className="name">Alan Key</span>
							<div className="annotation">
								<span>(creator of Smalltalk)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Lisp-enthusiast</li>
						<li>Emacs-friendly</li>
						<li>Implementor of an OOP model in Lisp</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Full of very interesting features</li>
						<li>Powerful yet elegant</li>
						<li><code className="code-container lisp">
							<span className="paren open">(</span>
							<span className="code">is-cool-p</span>
							<span className="paren close">)</span>
							<span className="comment">;; true</span>
						</code></li>
					</ul>
				</section>
			</div>
		</div>
	);
}
