import React from "react";

/**
 * @module populateScrum
 */
export default function populateScrum() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide linders">
					<div className="slide-quote">
						<div className="quote">
							Stable Velocity. Sustainable Pace.
						</div>
						<div className="author">
							<span className="name">Mike Cottmeyer</span>
							<div className="annotation">
								<span>LeadingAgile CEO and Founder</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Facilitator of agile retrospectives</li>
						<li>WorkflowMaster for several years</li>
						<li>Project leader</li>
					</ul>
				</section>
				<section className="slide linders">
					<div className="slide-quote">
						<div className="quote">
							Agile retrospectives give the power to the team, where it belongs!
						</div>
						<div className="author">
							<span className="name">Ben Linders</span>
							<div className="annotation">
								<span>Getting Value out of Agile Retrospectives</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Development is not a sprint but a marathon</li>
						<li>Steady velocity</li>
						<li>Self-satisfaction of moving on</li>
						<li>Short term planning</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
