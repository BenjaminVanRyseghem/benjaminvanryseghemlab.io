import React from "react";

/**
 * @module populateLess
 */
export default function populateLess() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide sass">
					<div className="slide-quote">
						<div className="quote">CSS with superpowers</div>
						<div className="author">
							<span className="name">Sass website</span>
							<div className="annotation">
								<span>(<a href="http://sass-lang.com/">http://sass-lang.com/</a>)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Several years of SCSS usage</li>
						<li>Several years of Less usage</li>
						<li>Mixins lover</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>A lot easier to read</li>
						<li>Nesting</li>
						<li>Variables</li>
						<li>Mixins</li>
						<li>Everything missing to CSS</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
