import React from "react";

/**
 * @module populateGit
 */
export default function populateGit() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide cats">
					<div className="slide-quote">
						<div className="quote">
							All your rebase are belong to us
						</div>
						<div className="author">
							<span className="name">CATS</span>
							<div className="annotation">
								<span>Zero Wing (more or less)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>git user for years</li>
						<li>Comprehension of git internals</li>
						<li>Proficient with git cli</li>
						<li>Commit early, commit often</li>
					</ul>
				</section>
				<section className="slide yoda">
					<div className="slide-quote">
						<div className="quote">
							May the forks be with you
						</div>
						<div className="author">
							<span className="name">Yoda</span>
							<div className="annotation">
								<span>(more or less)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Version Control System is a must-have</li>
						<li>Very flexible</li>
						<li>Not quite user friendly</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
