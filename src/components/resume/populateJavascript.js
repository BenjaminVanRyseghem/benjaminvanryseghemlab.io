import React from "react";

/**
 * @module populateJavascript
 */
export default function populateJavascript() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide crockford">
					<div className="slide-quote">
						<div className="quote">
							JavaScript is a language with more than its share of bad parts
						</div>
						<div className="author">
							<span className="name">Douglas Crockford</span>
							<div className="annotation">
								<span>JavaScript: The Good Parts</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Implementor of an OOP model in JS</li>
						<li>Daily use of the OOP Crockford style</li>
						<li>Main contributor of a widget-based UI framework</li>
					</ul>
				</section>
				<section className="slide ben">
					<div className="slide-quote">
						<div className="quote">
							With great power comes great responsibility
						</div>
						<div className="author">
							<span className="name">Uncle Ben</span>
							<div className="annotation">
								<span>(minutes before dying)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Good parts providing a lot of freedom</li>
						<li>Bad parts forcing to be careful</li>
						<li>Code clean or die</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
