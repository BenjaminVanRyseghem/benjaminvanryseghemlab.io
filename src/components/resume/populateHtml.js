import React from "react";

/**
 * @module populateHtml
 */
export default function populateHtml() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide design">
					<div className="slide-quote">
						<div className="quote">If you think math is hard, try web design!</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Implementor of SPAs</li>
						<li>Implementor of desktop apps in HTML/CSS</li>
						<li>Aware of backward compatibility (old IEs)</li>
						<li>Design responsive pages</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Really good rendering framework</li>
						<li>{"Can't get away with it"}</li>
						<li>CSS is style verbose</li>
						<li>Waiting for CSS4!</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
