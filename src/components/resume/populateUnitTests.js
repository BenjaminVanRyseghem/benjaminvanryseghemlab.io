import React from "react";

/**
 * @module populateUnitTests
 */
export default function populateUnitTests() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide ruskin">
					<div className="slide-quote">
						<div className="quote">
							Quality is never an accident; it is always the result of intelligent
							effort
						</div>
						<div className="author">
							<span className="name">John Ruskin</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li><span className="important">Very</span> familiar with SUnit</li>
						<li>Familiar with TDD, BDD, XP</li>
						<li>Experience with Jasmine, QUnit, Mocha, NUnit</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Everything should be covered by a test</li>
						<li>Regression should never occur</li>
						<li>{"\"Testable code is almost always better code.\" – Chad Fowler"}</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
