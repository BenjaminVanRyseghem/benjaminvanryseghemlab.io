import React from "react";

/**
 * @module populateRemote
 */
export default function populateRemote() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide anonymous">
					<div className="slide-quote">
						<div className="quote">
							Work with the bests, not the bests around.
						</div>
						<div className="author">
							<span className="name">Anonymous</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Remote my whole career</li>
						<li>FLOSS projects are the best remote schools</li>
						<li>English at work my whole career</li>
						<li>French native speaker</li>
					</ul>
				</section>
				<section className="slide">
					<ul>
						<li>Work with the best suited people, regardless of where they live</li>
						<li>More freedom brings more creativity and motivation</li>
						<li>Cultural mixing are a boost</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
