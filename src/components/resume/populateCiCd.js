import React from "react";

/**
 * @module populateCiCd
 */
export default function populateCiCd() {
	return (
		<div className="content-wrapper">
			<div className="content">
				<section className="slide humble">
					<div className="slide-quote">
						<div className="quote">
							If it hurts, do it more frequently, and bring the pain forward.
						</div>
						<div className="author">
							<span className="name">Jez Humble</span>
							<div className="annotation">
								<span>(about continuous delivery)</span>
							</div>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>Tooling responsible for years</li>
						<li>Strong experience with Jenkins, Travis, TeamCity, GitLab CI/CD</li>
						<li>Migrate from grunt to gulp</li>
						<li>Implementation of gulp pipelines</li>
					</ul>
				</section>
				<section className="slide farley">
					<div className="slide-quote">
						<div className="quote">
							The earlier you catch defects, the cheaper they are to fix.
						</div>
						<div className="author">
							<span className="name">David Farley</span>
						</div>
					</div>
				</section>
				<section className="slide">
					<ul>
						<li>{"Don't trust a human to do a bot job"}</li>
						<li>Ship as often as you can</li>
						<li>Manual steps never scale</li>
					</ul>
				</section>
			</div>
		</div>
	);
}
