import PropTypes from "prop-types";
import React from "react";

export default class LightBox extends React.Component {
	static propTypes = {
		image: PropTypes.string.isRequired,
		title: PropTypes.string.isRequired
	};

	state = {};

	render() {
		return (
			<a
				className="popup-link"
				data-lightbox={this.props.title}
				data-title={this.props.title}
				href={this.props.image}
				title={this.props.title}
			>
				<figure>
					<img alt={this.props.title} src={this.props.image}/>
					<figcaption>{this.props.title}</figcaption>
				</figure>
			</a>
		);
	}
}
