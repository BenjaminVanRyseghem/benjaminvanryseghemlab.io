import moment from "moment";
import PropTypes from "prop-types";
import React from "react";
import ReactMarkdown from "react-markdown";

export default class AlbumsList extends React.Component {
	static propTypes = { albums: PropTypes.array.isRequired };

	renderAlbum(album) {
		if (album.isAfter(new Date())) {
			return null;
		}
		return (
			<li key={album.artist()} className="blog-article album">
				<div className="clearfix" id={album.artist()}>
					<h3>[Week {album.weekNumber()}] {album.album()} - {album.artist()}</h3>
					<div className="post post-date">{moment(album.date()).format("MMMM DD, YYYY")}</div>
				</div>
				<div className="article-image">
					<div className="cover-wrapper">
						<a href={album.url()}>
							<figure className="album-cover">
								<img src={`/images/post/albums/${album.formattedDate()}.jpg`}/>
								<figcaption>Listen on Youtube</figcaption>
							</figure>
						</a>
					</div>
				</div>
				<div className="article-preview">
					<div><ReactMarkdown>{`**Favorite song:**  ${album.song()}`}</ReactMarkdown></div>
					<div><ReactMarkdown>{`**Description:**  ${album.description()}`}</ReactMarkdown></div>
					<div><ReactMarkdown>{`**How I discovered it?**  ${album.discovered()}`}</ReactMarkdown></div>
				</div>
			</li>
		);
	}

	render() {
		return (
			<ul className="articles">
				{this.props.albums.sort().map((album) => this.renderAlbum(album))}
			</ul>
		);
	}
}
