import React from "react";
import ReactDOM from "react-dom";

/* jQuery */
/* eslint-disable max-statements */
export default function drawThermometerDetails(id, details) {
	let lineWidth = 4;
	let radius = 150;
	let lineLength = 40;
	let popupHeight = 340;
	let popupWidth = 300;

	let container = jQuery(`#container-${id}`);

	function buildId(key, skill) {
		return `${key}-${id}-${skill.replace(" ", "-")}`;
	}

	function buildCanvasId(key, skill) {
		return buildId(`canvas-${key}`, skill);
	}

	function buildPopupId(key, skill) {
		return buildId(`popup-${key}`, skill);
	}

	function drawArc(skill, start, end) {
		let size = 2 * (radius + lineWidth) + 10;
		let $parent = jQuery(container).closest(".thermometer");
		let left = `${($parent.width() - size) / 2}px`;
		let top = `${($parent.height() - size) / 2}px`;

		let node = `<svg style="position: absolute; top: ${top}; left:${left};" width="${size}" height="${size}" class="arc" id="${buildCanvasId("arc", skill)}">`;

		let centerX = size / 2;
		let centerY = size / 2;

		let startAngle = (start * Math.PI / 50) - Math.PI / 2;
		let startX = centerX + (radius * Math.cos(startAngle));
		let startY = centerY + (radius * Math.sin(startAngle));

		let endAngle = (end * Math.PI / 50) - Math.PI / 2;
		let endX = centerX + radius * Math.cos(endAngle);
		let endY = centerY + radius * Math.sin(endAngle);

		let flags = " 0 0 1 ";
		node += `<path stroke-width="${lineWidth}" class="details-arc ${skill}" d="`;
		node += `M ${startX} ${startY} A ${radius} ${radius}${flags}${endX} ${endY}`;
		node += "\"></path>";
		node += "</svg>";

		jQuery(node).appendTo(container)
			.get(0);
	}

	function drawLine(skill, start, end) {
		let size = (2 * lineLength) + 10;

		let $parent = jQuery(container).closest(".thermometer");

		let midPercent = (start + end) / 2;
		let midAngle = (midPercent * Math.PI / 50) - Math.PI / 2;
		let className = "";

		let startX = 0;
		let startY = 0;
		let left = 0;
		let right = 0;
		let top = 0;
		let bottom = 0;

		if (midPercent >= 0 && midPercent < 25) { // top right
			startX = lineWidth;
			startY = size - lineWidth;
			className += " top right";
			left = `${$parent.width() / 2 + (radius * Math.cos(midAngle)) - lineWidth}px`;
			bottom = `${$parent.height() / 2 - (radius * Math.sin(midAngle)) - lineWidth}px`;
		} else if (midPercent >= 25 / 2 && midPercent < 50) { // bottom right
			startX = lineWidth;
			startY = lineWidth;
			className += " bottom right";
			left = `${$parent.width() / 2 + (radius * Math.cos(midAngle)) - lineWidth}px`;
			top = `${$parent.height() / 2 + (radius * Math.sin(midAngle)) - lineWidth}px`;
		} else if (midPercent >= 50 && midPercent < 75) { // bottom left
			startX = size - lineWidth;
			startY = lineWidth;
			className += " bottom left";
			right = `${$parent.width() / 2 - (radius * Math.cos(midAngle)) - lineWidth}px`;
			top = `${$parent.height() / 2 + (radius * Math.sin(midAngle)) - lineWidth}px`;
		} else if (midPercent >= 75 && midPercent < 100) { // top left
			startX = size - lineWidth;
			startY = size - lineWidth;
			className += " top left";
			right = `${$parent.width() / 2 - (radius * Math.cos(midAngle)) - lineWidth}px`;
			bottom = `${$parent.height() / 2 - (radius * Math.sin(midAngle)) - lineWidth}px`;
		}

		let midX = startX + (lineLength * Math.cos(midAngle));
		let midY = startY + (lineLength * Math.sin(midAngle));
		let offset = midPercent < 50 ? lineLength : -lineLength;

		let node = "<svg style=\"position: absolute; ";
		if (top) {
			node += "top: ";
			node += top;
		} else {
			node += "bottom: ";
			node += bottom;
		}
		node += "; ";
		if (left) {
			node += "left: ";
			node += left;
		} else {
			node += "right: ";
			node += right;
		}
		node += `;" width="${size}" height="${size}" class="line ${className}" id="${buildCanvasId("line", skill)}">`;
		node += `<path stroke-width="${lineWidth}" class="details-line ${skill}" d="`;
		node += `M ${startX} ${startY} L ${midX} ${midY} h ${offset}`;
		node += "\"></path>";
		node += "</svg>";

		jQuery(node).appendTo(container)
			.get(0);
	}

	function addPopup({ skill, name, popupContent, start, end }) {
		let div = jQuery(`<div class="popup" id="${buildPopupId("popup", skill)}"></div>`).appendTo(container)
			.get(0);
		div.style.position = "absolute";
		div.style.height = `${popupHeight}px`;
		div.style.width = `${popupWidth}px`;

		let midPercent = (start + end) / 2;
		let midAngle = (midPercent * Math.PI / 50) - Math.PI / 2;
		let $parent = jQuery(div).closest(".thermometer");

		let offset = midAngle > Math.PI / 2 ? -lineLength : lineLength;

		let startX = 0;
		let startY = 0;
		let anchorX = 0;
		let anchorY = 0;

		if (midPercent >= 0 && midPercent < 25) { // top right
			startX = 0;
			startY = popupHeight;

			anchorX = startX + (lineLength * Math.cos(midAngle)) + offset;
			anchorY = startY + (lineLength * Math.sin(midAngle));

			div.className += " top right";
			div.style.left = `${$parent.width() / 2 + (radius * Math.cos(midAngle)) + anchorX}px`;
			div.style.bottom = `${$parent.height() / 2 - (radius * Math.sin(midAngle)) - anchorY + (popupHeight / 2)}px`;
		} else if (midPercent >= 25 / 2 && midPercent < 50) { // bottom right
			startX = 0;
			startY = 0;

			anchorX = startX + (lineLength * Math.cos(midAngle)) + offset;
			anchorY = startY + (lineLength * Math.sin(midAngle));

			div.className += " bottom right";
			div.style.left = `${$parent.width() / 2 + (radius * Math.cos(midAngle)) + anchorX}px`;
			div.style.top = `${$parent.height() / 2 + (radius * Math.sin(midAngle)) + anchorY - (popupHeight / 2)}px`;
		} else if (midPercent >= 50 && midPercent < 75) { // bottom left
			startX = popupWidth;
			startY = 0;

			anchorX = startX + (lineLength * Math.cos(midAngle)) + offset;
			anchorY = startY + (lineLength * Math.sin(midAngle));

			div.className += " bottom left";
			div.style.right = `${$parent.width() / 2 - (radius * Math.cos(midAngle)) - anchorX + popupWidth}px`;
			div.style.top = `${$parent.height() / 2 + (radius * Math.sin(midAngle)) + anchorY - (popupHeight / 2)}px`;
		} else if (midPercent >= 75 && midPercent < 100) { // top left
			startX = popupWidth;
			startY = popupHeight;

			anchorX = startX + (lineLength * Math.cos(midAngle)) + offset;
			anchorY = startY + (lineLength * Math.sin(midAngle));

			div.className += " top left";
			div.style.right = `${$parent.width() / 2 - (radius * Math.cos(midAngle)) - anchorX + popupWidth}px`;
			div.style.bottom = `${$parent.height() / 2 - (radius * Math.sin(midAngle)) - anchorY + (popupHeight / 2)}px`;
		}

		ReactDOM.render(<>
			<div className={`popup-title ${skill}`}>{name}</div>
			{popupContent}
		</>, div);
	}

	let acc = 0;

	Object.keys(details).forEach((skill) => {
		let data = details[skill];
		let popupContent = data.popup;
		let { name } = data;

		let start = acc;
		let end = start + data.value;

		drawArc(skill, start, end);
		drawLine(skill, start, end);
		addPopup({
			skill,
			name,
			popupContent,
			start,
			end
		});

		acc += data.value;
	});
}
