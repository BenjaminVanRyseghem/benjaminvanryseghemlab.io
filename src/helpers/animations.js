/* global TweenMax,TimelineMax,ScrollMagic */
/* eslint-disable max-lines,new-cap */

let greenColor = [136, 224, 95];
let orangeColor = [224, 206, 113];
let redColor = [224, 91, 94];

let data = {
	oop: ["st", "js", "dp"],
	fp: ["lisp", "clojure", "caml"],
	web: ["html", "less-scss", "svg"],
	agile: ["unit-tests", "scrum", "pomodoro", "oneflow"],
	tools: ["remote", "git", "gtd", "tasks", "devops"]
};

const thermometerDuration = 5000;

function colorChannelMixer(colorChannelA, colorChannelB, amountToMix) {
	let channelA = colorChannelA * amountToMix;
	let channelB = colorChannelB * (1 - amountToMix);
	return parseInt(channelA + channelB, 10);
}

/*
 * rgbA and rgbB are arrays, amountToMix ranges from 0.0 to 1.0
 * example (red): rgbA = [255,0,0]
 */
function colorMixer(rgbA, rgbB, amountToMix) {
	let red = colorChannelMixer(rgbA[0], rgbB[0], amountToMix);
	let green = colorChannelMixer(rgbA[1], rgbB[1], amountToMix);
	let blue = colorChannelMixer(rgbA[2], rgbB[2], amountToMix);
	return `rgb(${red},${green},${blue})`;
}

function arrayToRGB(array) {
	return `rgb(${array[0]}, ${array[1]}, ${array[2]})`;
}

function init() { }

/*
 *
 * Tweens
 *
 */

function thermometerActivation(key) {
	let percent = +jQuery(key).attr("data-percent");
	let tweens = [];

	tweens.push(new TweenMax.fromTo(
		`${key} .p${percent} .bar`, 100,
		{ drawSVG: "0%" },
		{ drawSVG: "100%" }
	));

	let endColor = null;
	if (percent < 50) {
		endColor = colorMixer(orangeColor, redColor, percent / 50);

		tweens.push(new TweenMax.fromTo(
			`${key} .p${percent} .bar`, 100,
			{ stroke: arrayToRGB(redColor) },
			{ stroke: endColor }
		));
	} else {
		endColor = colorMixer(greenColor, orangeColor, (percent - 50) / 50);

		let timeline = new TimelineMax();
		timeline.add(new TweenMax.fromTo(
			`${key} .p${percent} .bar`, 50,
			{ stroke: arrayToRGB(redColor) },
			{ stroke: arrayToRGB(orangeColor) }
		));

		timeline.add(new TweenMax.to(`${key} .p${percent} .bar`, 50, {
			stroke: endColor
		}));

		tweens.push(timeline);
	}

	return tweens;
}

function centerThermometer(key) {
	return TweenMax.to(`#${key}`, 300, {
		left: "50%",
		x: "-50%"
	});
}

function uncenterThermometer(key) {
	let index = 0;
	switch (key) {
		case "oop":
			index = 0;
			break;
		case "fp":
			index = 1;
			break;
		case "web":
			index = 2;
			break;
		case "agile":
			index = 3;
			break;
		case "tools":
			index = 4;
			break;
	}
	return TweenMax.to(`#${key}`, 300, {
		left: `${index * 20}%`,
		x: "0%"
	});
}

function showThermometer(key) {
	return TweenMax.to(`#${key}`, 100, { opacity: 1 });
}

function hideThermometer(key) {
	return TweenMax.to(`#${key}`, 100, { opacity: 0 });
}

function moveTitleAndZoom(key) {
	let zoomSlice = TweenMax.to(`#${key} .slice`, 200, {
		scale: 2
	});
	let zoomPercent = TweenMax.to(`#${key} .percent`, 200, {
		fontSize: 60
	});

	let title = TweenMax.to(`#${key} .title`, 100, {
		top: "-240px",
		fontSize: "80px",
		onStart() {
			jQuery(`#${key} .thermometer-content`).width(640);
		},
		onReverseComplete() {
			jQuery(`#${key} .thermometer-content`).width(140);
		}
	});

	return [zoomSlice, zoomPercent, title];
}

function unmoveTitleAndUnzoom(key) {
	let zoomSlice = TweenMax.to(`#${key} .slice`, 150, {
		scale: 1
	});
	let zoomPercent = TweenMax.to(`#${key} .percent`, 150, {
		fontSize: 30
	});

	let title = TweenMax.to(`#${key} .title`, 150, {
		top: "140px",
		fontSize: "40px",
		onComplete() {
			jQuery(`#${key} .thermometer-content`).width(140);
		},
		onReverseComplete() {
			jQuery(`#${key} .thermometer-content`).width(640);
		}
	});

	return [zoomSlice, zoomPercent, title];
}

function showThermometerDetails(key) {
	return TweenMax.fromTo(
		`#${key} .details-arc`, 60,
		{ drawSVG: "0%" },
		{ drawSVG: "100%" }
	);
}

function hideThermometerDetails(key) {
	return TweenMax.to(`#${key} .details-arc`, 60, {
		drawSVG: "0%"
	});
}

function showThermometerLine(key, skill) {
	return TweenMax.fromTo(
		`#${key} .details-line.${skill}`, 60,
		{ drawSVG: "0%" },
		{ drawSVG: "100%" }
	);
}

function hideThermometerLine(key, skill) {
	return TweenMax.to(`#${key} .details-line.${skill}`, 60, {
		drawSVG: "0%"
	});
}

function showThermometerPopup(key, skill) {
	return TweenMax.to(`#popup-popup-${key}-${skill}`, 60, {
		opacity: 1,
		onStart() {
			jQuery(`#popup-popup-${key}-${skill}`).addClass("active");
		},
		onReverseComplete() {
			jQuery(`#popup-popup-${key}-${skill}`).removeClass("active");
		}
	});
}

function hideThermometerPopup(key, skill) {
	return TweenMax.to(`#popup-popup-${key}-${skill}`, 60, {
		opacity: 0,
		onStart() {
			jQuery(`#popup-popup-${key}-${skill}`).removeClass("active");
		},
		onReverseComplete() {
			jQuery(`#popup-popup-${key}-${skill}`).addClass("active");
		}
	});
}

/*
 *
 * Setup
 *
 */

function setupThermometerActivation(allSkills, offset) {
	let tl = new TimelineMax();

	tl.add(allSkills.map((key) => thermometerActivation(`#${key}`)));

	let controller = new ScrollMagic.Controller();
	new ScrollMagic.Scene({
		triggerElement: "#scroll-trigger",
		offset,
		duration: 300
	})
		.setTween(tl)

		// .addIndicators({name: 'thermometer activation'})
		.addTo(controller);

	return 300;
}

function setupSlides(key, skill) {
	let identifier = `#popup-popup-${key}-${skill} .content`;
	let container = jQuery(identifier);
	let numberOfSlides = container.find("section.slide").length;
	let timeline = new TimelineMax();
	timeline.to(identifier, 60, { top: 0 });

	for (let i = 1; i < numberOfSlides; i++) {
		timeline.to(identifier, 100, { top: -300 * i });
		timeline.to(identifier, i === (numberOfSlides - 1) ? 100 : 30, { top: -300 * i });
	}

	return timeline;
}

function setupThermometer({ key, skills, allKeys, offset }) {
	let otherKeys = allKeys.filter((each) => each !== key);

	let tl = new TimelineMax();

	tl.add(otherKeys.map((otherKey) => hideThermometer(otherKey)));

	tl.add(centerThermometer(key));
	tl.add(moveTitleAndZoom(key));
	tl.add(showThermometerDetails(key));

	skills.forEach((skill) => {
		tl.add([
			showThermometerLine(key, skill),
			showThermometerPopup(key, skill)
		]);
		tl.add(setupSlides(key, skill));
		tl.add([
			hideThermometerLine(key, skill),
			hideThermometerPopup(key, skill)
		]);
	});

	tl.add(hideThermometerDetails(key));
	tl.add(unmoveTitleAndUnzoom(key));
	tl.add(uncenterThermometer(key));

	tl.add(otherKeys.map((otherKey) => showThermometer(otherKey)));

	let controller = new ScrollMagic.Controller();
	new ScrollMagic.Scene({
		triggerElement: "#scroll-trigger",
		offset,
		duration: thermometerDuration
	})
		.setTween(tl)
		.on("enter", () => {
			jQuery(`#${key}`).addClass("active");
		})
		.on("leave", () => {
			jQuery(`#${key}`).removeClass("active");
		})

		// .addIndicators({name: key})
		.addTo(controller);

	return thermometerDuration;
}

export default function animations() {
	let offset = 0;
	let allKeys = Object.keys(data);

	init();
	offset += setupThermometerActivation(allKeys, offset);

	allKeys.forEach((key) => {
		let skills = data[key];
		offset += setupThermometer({
			key,
			skills,
			allKeys,
			offset
		});
	});

	let totalHeight = $(window).height();
	let thermometersHeight = 200;
	let offsetY = (totalHeight - thermometersHeight) / 2;
	let ratio = offsetY / totalHeight;
	let endOffset = (0.5 - ratio) * totalHeight;

	let controller = new ScrollMagic.Controller();
	new ScrollMagic.Scene({
		triggerElement: "#scroll-trigger",
		triggerHook: ratio,
		duration: offset - endOffset
	})
		.setPin("#skills-move")

		// .addIndicators({name: 'pin'})
		.addTo(controller);

	new ScrollMagic.Scene({
		triggerElement: "#timeline",
		duration: 1
	})
		.setTween(TweenMax.to("#scroll-to-top", 1, {
			visibility: "visible"
		}))

		// .addIndicators({name: 'scroll-to-top'})
		.addTo(controller);
}
