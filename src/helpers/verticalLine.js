/**
 * @module verticalLine
 */

export default function verticalLine() {
	let timelineBlocks = $(".timeline-block");
	let offset = 300;

	// hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks);

	// on scolling, show/animate timeline blocks when enter the viewport
	$(window).on("scroll", () => {
		window.requestAnimationFrame
			? window.requestAnimationFrame(() => { showBlocks(timelineBlocks, offset); })
			: setTimeout(() => { showBlocks(timelineBlocks, offset); }, 100);
	});

	function hideBlocks(blocks) {
		blocks.find(".timeline-img, .timeline-content").addClass("is-hidden");
	}

	function showBlocks(blocks, delta) {
		blocks.each(function fn() {
			// eslint-disable-next-line no-invalid-this
			let block = $(this);

			let currentPosition = $(window).scrollTop() + $(window).height() - delta;
			if (block.offset().top <= currentPosition && block.find(".timeline-img").hasClass("is-hidden")) {
				block.find(".timeline-img, .timeline-content").removeClass("is-hidden")
					.addClass("bounce-in");
			}
		});
	}
}
