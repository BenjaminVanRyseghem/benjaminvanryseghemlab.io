import { compiler } from "markdown-to-jsx";
import React from "react";

/**
 * @module jsxMd
 */
export default function jsxMd(strings, ...subs) {
	return strings.reduce((acc, content, index) => {
		acc.push(compiler(content));
		subs[index] && acc.push(subs[index]);
		return acc;
	}, []);
}
