/**
 * @module getAge
 */
export default function getAge() {
	let now = new Date();
	let age = now.getFullYear() - 1987;
	if ((now.getMonth() < 9) || (now.getMonth() === 9 && now.getDate() < 17)) {
		return age - 1;
	}

	return age;
}
