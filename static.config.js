/* eslint-disable filenames/match-regex,filenames/match-exported */
import Album from "./src/models/album.js";
import Article from "./src/models/article";
import fs from "fs";
import path from "path";
import React from "react";

function loadFilesIn(folder) {
	return new Promise((resolve, reject) => {
		fs.readdir(folder, (err, files) => {
			if (err) {
				reject(err);
				return;
			}

			let promises = files.map((file) => import(path.resolve(folder, file)).then((module) => module.default));

			resolve(Promise.all(promises));
		});
	});
}

const config = {
	siteRoot: "https://benjamin.vanryseghem.com",
	// eslint-disable-next-line max-params,react/display-name,react/prop-types
	Document: ({ Html, Head, Body, children }) => (
		<Html lang="en-US">
			<Head>
				<title>Benjamin Van Ryseghem</title>
				<link href="/favicon.png" rel="shortcut icon" type="image/x-icon"/>
				<meta charSet="UTF-8"/>
				<link href="/lib/leaflet/leaflet.css" rel="stylesheet" type="text/css"/>
				<link href="/lib/lightbox2/css/lightbox.css" rel="stylesheet" type="text/css"/>
				<script src="/lib/jquery/jquery.min.js"/>
				<script src="/lib/leaflet/leaflet.js"/>
				<script src="/lib/scrollmagic/ScrollMagic.min.js"/>
				<script src="/lib/greensock-js/TweenMax.js"/>
				<script src="/lib/greensock-js/TimelineMax.min.js"/>
				<script src="/lib/drawSVG.js"/>
				<script src="/lib/scrollmagic/plugins/debug.addIndicators.min.js"/>
				<script src="/lib/scrollmagic/plugins/animation.gsap.min.js"/>
				<meta content="width=device-width, initial-scale=1" name="viewport"/>
			</Head>
			<Body>
				{children}
				<script src="/lib/lightbox2/js/lightbox.min.js"/>
			</Body>
		</Html>
	),
	getRoutes: async () => {
		let albumsFiles = await loadFilesIn(path.resolve("./data/albums"));
		let articleFiles = await loadFilesIn(path.resolve("./data/articles"));
		let albums = albumsFiles
			.map((data) => new Album(data))
			.filter((each) => !each.isDraft());
		let articles = articleFiles
			.map((data) => new Article(data))
			.filter((each) => !each.isDraft());
		return [
			{
				path: "/blog",
				getData: () => ({
					albums: albums.map((each) => each.toRaw()),
					articles: articles.map((each) => each.toRaw())
				}),
				children: [...albums, ...articles].map((entry) => ({
					path: `/${entry.category()}/${entry.urlSegment()}`,
					template: "src/templates/article",
					getData: () => ({
						entry: entry.toRaw()
					})
				}))
			}
		];
	},
	plugins: [
		[
			require.resolve("react-static-plugin-source-filesystem"),
			{
				location: path.resolve("./src/pages")
			}
		],
		require.resolve("react-static-plugin-reach-router"),
		require.resolve("react-static-plugin-sitemap"),
		require.resolve("react-static-plugin-sass")
	]
};

export default config;
