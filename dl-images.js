let fs = require("fs");
let path = require("path");

const urlRegexp = /^\s*url:\s*"(.*)",\s*$/i;
const dateRegexp = /^\s*date:\s*"([^"]*)",\s*$/i;

const glob = require("glob");
const cheerio = require("cheerio");
const request = require("request");
const src = "./";
const dest = path.resolve(__dirname, "public", "images", "post", "albums");
const source = path.resolve(__dirname, "public", "images", "post", "albums");
const puppeteer = require("puppeteer");

const download = (url, path, pageUrl) => new Promise((resolve, reject) => {
	request.head(url, (err, res, body) => {
		if (err) {
			console.log("pageUrl", pageUrl);
			return;
		}
		request(url)
			.on("error", (error) => {
				reject(error);
			})
			.pipe(fs.createWriteStream(path))
			.on("error", (error) => {
				console.log("url", url);
				reject(error);
			})
			.on("close", resolve);
	});
});
const downloadImage = (url, name) => {
	if (fs.existsSync(path.resolve(source, `${name}.jpg`))) {
		return Promise.resolve();
	}
	let finalDestination = path.resolve(dest, `${name}.jpg`);
	console.log("Download image to: ", finalDestination);
	return puppeteer
		.launch({ args: ["--no-sandbox", "--disable-setuid-sandbox"] })
		.then((browser) => browser.newPage())
		.then((page) => page.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:81.0) Gecko/20100101 Firefox/81.0")
			.then(() => page))
		.then((page) => page.goto(url, { waitUntil: "networkidle0" }).then(() => page.content()))
		.then((html) => {
			let $ = cheerio.load(html);
			return $("img.yt-img-shadow#img").attr("src");
		})
		.then((imageUrl) => download(imageUrl, finalDestination, url))
		.catch((err) => {
			// handle error
			console.error(url, err);
		});
};

const run = () => new Promise((resolve, reject) => {
	fs.mkdirSync(path.resolve(dest), { recursive: true });
	glob(`${src}data/albums/**/*.js`, (err, files) => {
		if (err) {
			console.error(err);
			reject(err);
		}

		let promises = [];
		files.forEach((file) => {
			const data = fs.readFileSync(file, "UTF-8");
			const lines = data.split(/\r?\n/);
			let dateLine = lines.find((line) => line.match(dateRegexp));
			let date = dateLine.match(dateRegexp)[1];
			let urlLine = lines.find((line) => line.match(urlRegexp));
			let url = urlLine.match(urlRegexp)[1];
			promises.push(() => downloadImage(url, date));
		});

		return resolve(promises.reduce((acc, curr) => acc.finally(() => curr()), Promise.resolve()));
	});
})
	.then(() => {
		console.log("done");
		process.exit(0);
	});

run();
