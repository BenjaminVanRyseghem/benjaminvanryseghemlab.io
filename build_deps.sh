#!/bin/bash
name=$1
toplevel=$2
path=$3

echo [$name] Copying $toplevel/$path to $toplevel/public/projects
mkdir -p $toplevel/public/projects
cp -r $toplevel/$path $toplevel/public/projects
